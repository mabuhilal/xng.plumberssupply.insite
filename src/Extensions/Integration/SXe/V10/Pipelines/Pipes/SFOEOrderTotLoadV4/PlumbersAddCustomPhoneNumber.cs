﻿namespace Extensions.Integration.SXe.V10.Pipelines.Pipes.SFOEOrderTotLoadV4
{
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Integration.Connector.SXe.V10.Pipelines.Parameters;
    using Insite.Integration.Connector.SXe.V10.Pipelines.Results;

    public sealed class PlumbersAddCustomPhoneNumber : IPipe<SFOEOrderTotLoadV4Parameter, SFOEOrderTotLoadV4Result>
    {
        public int Order => 502;

        public SFOEOrderTotLoadV4Result Execute(IUnitOfWork unitOfWork, SFOEOrderTotLoadV4Parameter parameter, SFOEOrderTotLoadV4Result result)
        {
            parameter.JobLogger?.Debug($"{nameof(PlumbersAddCustomPhoneNumber)} Started.");
            result.SFOEOrderTotLoadV4Request.Ininheader[0].BillToPhone = parameter.CustomerOrder.GetProperty(PlumbersConstants.PlacedByPhoneNumberKey, string.Empty);
            parameter.JobLogger?.Debug($"{nameof(PlumbersAddCustomPhoneNumber)} Finished.");
            return result;
        }        
    }
}