﻿namespace Extensions.Integration.SXe.V10.Pipelines.Pipes.SFOEOrderTotLoadV4
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;

    using Insite.Common.Dependencies;
    using Insite.Common.Helpers;
    using Insite.Common.Logging;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Core.SystemSetting.Groups.Integration;
    using Insite.Data.Entities;
    using Insite.Integration.Connector.SXe.V10;
    using Insite.Integration.Connector.SXe.V10.Pipelines.Parameters;
    using Insite.Integration.Connector.SXe.V10.Pipelines.Results;
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.SFOEOrderTotLoadV4;

    using Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange;
    using Extensions.Integration.SXe.V10.Services;
    using Extensions.SystemSettings.Group;

    public class PlumbersModifyPlacedBy : IPipe<SFOEOrderTotLoadV4Parameter, SFOEOrderTotLoadV4Result>
    {
        private IDependencyLocator dependencyLocator;
        private PlumbersIntegrationSettings plumbersIntegrationSettings;

        public int Order => 1175;

        public PlumbersModifyPlacedBy(
            IDependencyLocator dependencyLocator,
            PlumbersIntegrationSettings plumbersIntegrationSettings)
        {
            this.dependencyLocator = dependencyLocator;
            this.plumbersIntegrationSettings = plumbersIntegrationSettings;
        }

        public SFOEOrderTotLoadV4Result Execute(IUnitOfWork unitOfWork, SFOEOrderTotLoadV4Parameter parameter, SFOEOrderTotLoadV4Result result)
        {
            if (!parameter.IsOrderSubmit)
            {
                return result;
            }

            parameter.JobLogger?.Debug($"{nameof(PlumbersModifyPlacedBy)} Started.");
            parameter.JobLogger?.Debug($"{nameof(SFOEOrderTotLoadV4Request)}: {this.GetSerializedValue(result.SFOEOrderTotLoadV4Request)}");

            var orderNumberParts = result.ErpOrderNumber?.Split('-');

            if (orderNumberParts != null && orderNumberParts.Length == 2)
            {
                long orderNumber;
                int orderNumberSuffix;

                if (long.TryParse(orderNumberParts[0], out orderNumber) && int.TryParse(orderNumberParts[1], out orderNumberSuffix))
                {
                    try
                    {
                        var defaultApprovalType = this.plumbersIntegrationSettings.DefaultOrderApprovalType;
                        var approvalType = parameter.CustomerOrder.Customer?.GetProperty(PlumbersConstants.ApprovalTypeKey, defaultApprovalType) ?? defaultApprovalType;
                        
                        parameter.JobLogger?.Debug($"{nameof(SFOEOrderTotLoadV4Result)} Started.");

                        if (result.ErpOrderNumber == null)
                        {
                            parameter.JobLogger?.Debug($"{nameof(SFOEOrderTotLoadV4Result)} Finished.");
                            return result;
                        }

                        var obj = new OEOrderChangeRequest();
                        obj.IncludeLines = false;
                        obj.OrderNumber = long.Parse(result.ErpOrderNumber.Substring(0, result.ErpOrderNumber.IndexOf('-')));
                        obj.OrderSuffix = 0;

                        parameter.JobLogger?.Debug($"{nameof(OEOrderChangeRequest)} 1: {PlumbersSXeApiServiceV10.GetSerializedValue(obj)}");

                        var response = this.dependencyLocator.GetInstance<IPlumbersSXeApiServiceFactory>()
                            .GetSXeApiServiceV10(parameter.IntegrationConnection)
                            .OEOrderChange(obj);

                        if (!string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            parameter.JobLogger?.Debug($"Error: {nameof(OEOrderChangeResponse)}: {response.ErrorMessage}");
                        }

                        parameter.JobLogger?.Debug($"{nameof(OEOrderChangeResponse)} 1: {PlumbersSXeApiServiceV10.GetSerializedValue(response)}");

                        foreach (var item in response.Outoutheaderextra)
                        {
                            var inHe = new Ininheaderextra() { FieldName = item.FieldName, FieldValue = item.FieldValue, SequenceNumber = item.SequenceNumber };
                            obj.Ininheaderextra.Add(inHe);
                        }

                        obj.Ininheader = new List<Ininheader>() { };

                        foreach (var item in response.Outoutheader)
                        {
                            var inH = new Ininheader()
                            {
                                ActualFreight = item.ActualFreight,
                                AddonAmount1 = item.AddonAmount1,
                                AddonAmount2 = item.AddonAmount2,
                                AddonAmount3 = item.AddonAmount3,
                                AddonAmount4 = item.AddonAmount4,
                                AddonAmount5 = item.AddonAmount5,
                                AddonAmount6 = item.AddonAmount6,
                                AddonAmount7 = item.AddonAmount7,
                                AddonAmount8 = item.AddonAmount8,
                                AddonDescription1 = item.AddonDescription1,
                                AddonDescription2 = item.AddonDescription2,
                                AddonDescription3 = item.AddonDescription3,
                                AddonDescription4 = item.AddonDescription4,
                                AddonDescription5 = item.AddonDescription5,
                                AddonDescription6 = item.AddonDescription6,
                                AddonDescription7 = item.AddonDescription7,
                                AddonDescription8 = item.AddonDescription8,
                                AddonNetAmount1 = item.AddonNetAmount1,
                                AddonNetAmount2 = item.AddonNetAmount2,
                                AddonNetAmount3 = item.AddonNetAmount3,
                                AddonNetAmount4 = item.AddonNetAmount4,
                                AddonNetAmount5 = item.AddonNetAmount5,
                                AddonNetAmount6 = item.AddonNetAmount6,
                                AddonNetAmount7 = item.AddonNetAmount7,
                                AddonNetAmount8 = item.AddonNetAmount8,
                                AddonNumber1 = item.AddonNumber1,
                                AddonNumber2 = item.AddonNumber2,
                                AddonNumber3 = item.AddonNumber3,
                                AddonNumber4 = item.AddonNumber4,
                                AddonNumber5 = item.AddonNumber5,
                                AddonNumber6 = item.AddonNumber6,
                                AddonNumber7 = item.AddonNumber7,
                                AddonNumber8 = item.AddonNumber8,
                                AddonType1 = item.AddonType1,
                                AddonType2 = item.AddonType2,
                                AddonType3 = item.AddonType3,
                                AddonType4 = item.AddonType4,
                                AddonType5 = item.AddonType5,
                                AddonType6 = item.AddonType6,
                                AddonType7 = item.AddonType7,
                                AddonType8 = item.AddonType8,
                                ApprovalType = approvalType,
                                ApproveInitials = item.ApproveInitials,
                                BackorderExistsFlag = item.BackorderExistsFlag,
                                BackorderFlag = item.BackorderFlag,
                                BackorderReleaseFlag = item.BackorderReleaseFlag,
                                BackorderStage = item.BackorderStage,
                                BillDate = item.BillDate,
                                CancelDate = item.CancelDate,
                                CityCode = item.CityCode,
                                CodCollectedAmount = item.CodCollectedAmount,
                                CodFlag = item.CodFlag,
                                ContactID = item.ContactID,
                                ContactName = item.ContactName,
                                CountryCode = item.CountryCode,
                                CreditMemoReasonType = item.CreditMemoReasonType,
                                CustomerAddress1 = item.CustomerAddress1,
                                CustomerAddress2 = item.CustomerAddress2,
                                CustomerAddress3 = item.CustomerAddress3,
                                CustomerCity = item.CustomerCity,
                                CustomerName = item.CustomerName,
                                CustomerNumber = item.CustomerNumber,
                                CustomerPhoneNumber = item.CustomerPhoneNumber,
                                CustomerPurchaseOrder = item.CustomerPurchaseOrder,
                                CustomerState = item.CustomerState,
                                CustomerZipCode = item.CustomerZipCode,
                                DatcOverrideFlag = item.DatcOverrideFlag,
                                DirectRouteDeliveryDate = item.DirectRouteDeliveryDate,
                                DirectRouteDeliveryTime = item.DirectRouteDeliveryTime,
                                DirectRouteExportFlag = item.DirectRouteExportFlag,
                                DirectRouteHoldFlag = item.DirectRouteHoldFlag,
                                DownPaymentAmount = item.DownPaymentAmount,
                                EnterDate = item.EnterDate,
                                Filler1 = item.Filler1,
                                Filler2 = item.Filler2,
                                Filler3 = item.Filler3,
                                FloorPlanCustomerNumber = item.FloorPlanCustomerNumber,
                                InBoundFreightFlag = item.InBoundFreightFlag,
                                InvoiceDate = item.InvoiceDate,
                                InvoiceNumber = item.InvoiceNumber,
                                InvoiceSuffix = item.InvoiceSuffix,
                                JobNumber = item.JobNumber,
                                JrnlNumber2 = item.JrnlNumber2,
                                LanguageCode = item.LanguageCode,
                                LastUpdate = item.LastUpdate,
                                LineFlag = item.LineFlag,
                                LockFlag = item.LockFlag,
                                LostBusinessType = item.LostBusinessType,
                                LumpBillingAmount = item.LumpBillingAmount,
                                LumpBillingFlag = item.LumpBillingFlag,
                                LumpPriceFlag = item.LumpPriceFlag,
                                NextLineNumber = item.NextLineNumber,
                                NonTaxType = item.NonTaxType,
                                NumberDirectOrderLines = item.NumberDirectOrderLines,
                                NumberLineItems = item.NumberLineItems,
                                NumberPackages = item.NumberPackages,
                                OrderDisposition = item.OrderDisposition,
                                OrderDispositionWord = item.OrderDispositionWord,
                                Other1Code = item.Other1Code,
                                Other2Code = item.Other2Code,
                                OutBoundFreightFlag = item.OutBoundFreightFlag,
                                PackageID = item.PackageID,
                                PaidDate = item.PaidDate,
                                PickCount = item.PickCount,
                                PickPrintFlag = item.PickPrintFlag,
                                PickedDate = item.PickedDate,
                                PickedInitials = item.PickedInitials,
                                PlacedBy = parameter.CustomerOrder.GetProperty(PlumbersConstants.PlacedByKey, string.Empty),
                                PmFlag = item.PmFlag,
                                PriceCode = item.PriceCode,
                                PromiseDate = item.PromiseDate,
                                ProposalNumber = item.ProposalNumber,
                                ProspectNumber = item.ProspectNumber,
                                PstLicenseNumber = item.PstLicenseNumber,
                                PurchasingAgentName = item.PurchasingAgentName,
                                PurchasingAgentPhoneNumber = item.PurchasingAgentPhoneNumber,
                                Reference = item.Reference,
                                RequestedShipDate = item.RequestedShipDate,
                                Route = item.Route,
                                SalesRepInside = item.SalesRepInside,
                                SalesRepOutside = item.SalesRepOutside,
                                ShipDate = item.ShipDate,
                                ShipTo = item.ShipTo,
                                ShipToAddress1 = item.ShipToAddress1,
                                ShipToAddress2 = item.ShipToAddress2,
                                ShipToAddress3 = item.ShipToAddress3,
                                ShipToCity = item.ShipToCity,
                                ShipToName = item.ShipToName,
                                ShipToState = item.ShipToState,
                                ShipToZip = item.ShipToZip,
                                ShipViaType = item.ShipViaType,
                                ShipViaTypeDescription = item.ShipViaTypeDescription,
                                ShippingInstructions = item.ShippingInstructions,
                                SoldToAddress1 = item.SoldToAddress1,
                                SoldToAddress2 = item.SoldToAddress2,
                                SoldToAddress3 = item.SoldToAddress3,
                                SoldToCity = item.SoldToCity,
                                SoldToState = item.SoldToState,
                                SoldToName = item.SoldToName,
                                SoldToZipCode = item.SoldToZipCode,
                                SpecialDiscountAmount = item.SpecialDiscountAmount,
                                StageCode = item.StageCode,
                                StageCodeWord = item.StageCodeWord,
                                StagingArea = item.StagingArea,
                                StandingOrderDays = item.StandingOrderDays,
                                StandingOrderType = item.StandingOrderType,
                                StateCode = item.StateCode,
                                SubstituteFlag = item.SubstituteFlag,
                                TakenBy = item.TakenBy,
                                TaxAmount1 = item.TaxAmount1,
                                TaxAmount2 = item.TaxAmount2,
                                TaxableFlag = item.TaxableFlag,
                                TaxAmount3 = item.TaxAmount3,
                                TaxAmount4 = item.TaxAmount4,
                                TaxAuthority = item.TaxAuthority,
                                TaxDefaultType = item.TaxDefaultType,
                                TaxOverrideCode = item.TaxOverrideCode,
                                TaxOverrideFlag = item.TaxOverrideFlag,
                                TenderAmount = item.TenderAmount,
                                TermsDiscountAmount = item.TermsDiscountAmount,
                                TermsPercent = item.TermsPercent,
                                TermsType = item.TermsType,
                                TermsTypeDescription = item.TermsTypeDescription,
                                TotalCostOrdered = item.TotalCostOrdered,
                                TotalCostShipped = item.TotalCostShipped,
                                TotalCubes = item.TotalCubes,
                                TotalInvoiceAmountOrdered = item.TotalInvoiceAmountOrdered,
                                TotalInvoiceAmountShipped = item.TotalInvoiceAmountShipped,
                                TotalLineAmountOrdered = item.TotalLineAmountOrdered,
                                TotalLineAmountShipped = item.TotalLineAmountShipped,
                                TotalQuantityOrdered = item.TotalQuantityOrdered,
                                TotalQuantityShipped = item.TotalQuantityShipped,
                                TotalWeight = item.TotalWeight,
                                TransactionType = item.TransactionType,
                                UseTaxwareStepDataFlag = item.UseTaxwareStepDataFlag,
                                User1 = item.User1,
                                User2 = item.User2,
                                User3 = item.User3,
                                User4 = item.User4,
                                User5 = item.User5,
                                User6 = item.User6,
                                User7 = item.User7,
                                User8 = item.User8,
                                User9 = item.User9,
                                VendorRebateAmount = item.VendorRebateAmount,
                                Warehouse = item.Warehouse,
                                WholeOrderDiscountAmount = item.WholeOrderDiscountAmount,
                                WholeOrderDiscountOverrideFlag = item.WholeOrderDiscountOverrideFlag,
                                WholeOrderDiscountPercent = item.WholeOrderDiscountPercent,
                                WholeOrderDiscountType = item.WholeOrderDiscountType
                            };
                            obj.Ininheader.Add(inH);
                        }

                        var responseFinal = this.dependencyLocator.GetInstance<IPlumbersSXeApiServiceFactory>()
                            .GetSXeApiServiceV10(parameter.IntegrationConnection)
                            .OEOrderChangeRequestFinal(obj);

                        if (!string.IsNullOrEmpty(responseFinal.ErrorMessage))
                        {
                            parameter.JobLogger?.Debug($"Error: {nameof(OEOrderChangeResponse)}: {responseFinal.ErrorMessage}");
                        }
  
                        return result;                       
                    }
                    catch(Exception ex)
                    {
                        LogHelper.For(this).Error($"An error occurred while setting order header value in ERP: {ex.Message} Inner exception: {ex.InnerException}");
                        return result;
                    }
                }
            }

            return result;
        }

        private string GetSerializedValue(object value)
        {
            var serializer = new XmlSerializer(value.GetType());
            var writer = new StringWriter();

            serializer.Serialize(writer, value);

            return writer.ToString();
        }

        internal PlumbersSXeSoapServiceV10 GetPlumbersSXeApiServiceV10(IUnitOfWork unitOfWork, IntegrationConnection integrationConnection = null)
        {
            integrationConnection = this.GetIntegrationConnection(integrationConnection, unitOfWork);

            var companyNumber = GetCompanyNumber(integrationConnection);
            var operatorPassword = GetOperatorPassword(integrationConnection);

            return new PlumbersSXeSoapServiceV10(integrationConnection.Url);
        }

        private static PlumbersSXeSoapServiceV10 CreateSXeSoapService(string baseUrl, params string[] apiPaths)
        {
            return new PlumbersSXeSoapServiceV10(CreateUrl(baseUrl, apiPaths));
        }

        private static string CreateUrl(string baseUrl, params string[] apiPaths)
        {
            var url = string.Empty;

            baseUrl = baseUrl.TrimEnd('/');

            foreach (var apiPath in apiPaths.Reverse())
            {
                if (baseUrl.EndsWith(apiPath, StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }

                url = $"{apiPath}/{url}";
            }

            url = $"{baseUrl}/{url}";

            return url.TrimEnd('/');
        }

        private IntegrationConnection GetIntegrationConnection(IntegrationConnection integrationConnection, IUnitOfWork unitOfWork)
        {
            if (integrationConnection != null)
            {
                return integrationConnection;
            }

            var sXeIntegrationConnectionId = this.dependencyLocator.GetInstance<IntegrationConnectorSettings>().SXeIntegrationConnectionId;
            if (sXeIntegrationConnectionId.IsBlank())
            {
                throw new ArgumentException("SXe Integration Connector requires an Integration Connection.");
            }

            integrationConnection = unitOfWork.GetRepository<IntegrationConnection>().Get(sXeIntegrationConnectionId);
            if (integrationConnection == null)
            {
                throw new ArgumentException("SXe Integration Connector requires an Integration Connection.");
            }

            return integrationConnection;
        }

        private static int GetCompanyNumber(IntegrationConnection integrationConnection)
        {
            int companyNumber;
            if (!int.TryParse(integrationConnection.SystemNumber, out companyNumber))
            {
                throw new ArgumentException("SXe Integration Connection company number must be in integer format.");
            }

            return companyNumber;
        }

        private static string GetOperatorPassword(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.Password);
        }

        private static string GetClientSecret(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.MessageServerService);
        }

        private static string GetPassword(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.GatewayService);
        }
    }
}