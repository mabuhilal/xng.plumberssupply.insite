﻿using System.Collections.Generic;

namespace Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange
{
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/NxT_API.com.infor.sxapi.OEOrderChange")]
    public class OEOrderChangeRequest
    {
        public bool IncludeLines { get; set; }
        public long OrderNumber { get; set; }

        public int OrderSuffix { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        public List<Ininheader> Ininheader { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        public List<Ininheaderextra> Ininheaderextra { get; set; }
    }

    public class Ininheaderextra
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public int SequenceNumber { get; set; }
    }

    public class Ininheader
    {
        public string ActualFreight { get; set; }
        public string AddonAmount1 { get; set; }
        public string AddonAmount2 { get; set; }
        public string AddonAmount3 { get; set; }
        public string AddonAmount4 { get; set; }
        public string AddonAmount5 { get; set; }
        public string AddonAmount6 { get; set; }
        public string AddonAmount7 { get; set; }
        public string AddonAmount8 { get; set; }
        public string AddonDescription1 { get; set; }
        public string AddonDescription2 { get; set; }
        public string AddonDescription3 { get; set; }
        public string AddonDescription4 { get; set; }
        public string AddonDescription5 { get; set; }
        public string AddonDescription6 { get; set; }
        public string AddonDescription7 { get; set; }
        public string AddonDescription8 { get; set; }

        public string AddonNetAmount1 { get; set; }
        public string AddonNetAmount2 { get; set; }
        public string AddonNetAmount3 { get; set; }
        public string AddonNetAmount4 { get; set; }
        public string AddonNetAmount5 { get; set; }
        public string AddonNetAmount6 { get; set; }
        public string AddonNetAmount7 { get; set; }
        public string AddonNetAmount8 { get; set; }
        public string AddonNumber1 { get; set; }
        public string AddonNumber2 { get; set; }
        public string AddonNumber3 { get; set; }
        public string AddonNumber4 { get; set; }
        public string AddonNumber5 { get; set; }
        public string AddonNumber6 { get; set; }
        public string AddonNumber7 { get; set; }
        public string AddonNumber8 { get; set; }
        public string AddonType1 { get; set; }
        public string AddonType2 { get; set; }
        public string AddonType3 { get; set; }
        public string AddonType4 { get; set; }
        public string AddonType5 { get; set; }
        public string AddonType6 { get; set; }
        public string AddonType7 { get; set; }
        public string AddonType8 { get; set; }
        public string ApprovalType { get; set; }
        public string ApproveInitials { get; set; }
        public bool BackorderExistsFlag { get; set; }
        public bool BackorderFlag { get; set; }
        public bool BackorderReleaseFlag { get; set; }
        public int BackorderStage { get; set; }
        public string BillDate { get; set; }
        public string CancelDate { get; set; }
        public string CityCode { get; set; }
        public int CodCollectedAmount { get; set; }
        public bool CodFlag { get; set; }
        public int ContactID { get; set; }
        public string ContactName { get; set; }
        public string CountryCode { get; set; }
        public string CountyCode { get; set; }
        public string CreditMemoReasonType { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerPurchaseOrder { get; set; }
        public string CustomerState { get; set; }
        public string CustomerZipCode { get; set; }
        public bool DatcOverrideFlag { get; set; }
        public string DirectRouteDeliveryDate { get; set; }
        public string DirectRouteDeliveryTime { get; set; }
        public bool DirectRouteExportFlag { get; set; }
        public bool DirectRouteHoldFlag { get; set; }
        public string DownPaymentAmount { get; set; }
        public string EnterDate { get; set; }
        public string Filler1 { get; set; }
        public string Filler2 { get; set; }
        public string Filler3 { get; set; }
        public string FloorPlanCustomerNumber { get; set; }
        public bool InBoundFreightFlag { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceSuffix { get; set; }
        public string JobNumber { get; set; }
        public string JrnlNumber2 { get; set; }
        public string LanguageCode { get; set; }
        public string LastUpdate { get; set; }
        public bool LineFlag { get; set; }
        public bool LockFlag { get; set; }
        public string LostBusinessType { get; set; }
        public string LumpBillingAmount { get; set; }
        public bool LumpBillingFlag { get; set; }
        public bool LumpPriceFlag { get; set; }
        public string NextLineNumber { get; set; }
        public string NonTaxType { get; set; }
        public string NumberDirectOrderLines { get; set; }
        public string NumberLineItems { get; set; }
        public string NumberPackages { get; set; }
        public string OrderDisposition { get; set; }
        public string OrderDispositionWord { get; set; }
        public string Other1Code { get; set; }
        public string Other2Code { get; set; }
        public bool OutBoundFreightFlag { get; set; }
        public string PackageID { get; set; }
        public string PaidDate { get; set; }
        public string PickCount { get; set; }
        public bool PickPrintFlag { get; set; }
        public string PickedDate { get; set; }
        public string PickedInitials { get; set; }
        public string PlacedBy { get; set; }
        public bool PmFlag { get; set; }
        public string PriceCode { get; set; }
        public string PromiseDate { get; set; }
        public string ProposalNumber { get; set; }
        public string ProspectNumber { get; set; }
        public string PstLicenseNumber { get; set; }
        public string PurchasingAgentName { get; set; }
        public string PurchasingAgentPhoneNumber { get; set; }
        public string Reference { get; set; }
        public string RequestedShipDate { get; set; }
        public string Route { get; set; }
        public string SalesRepInside { get; set; }
        public string SalesRepOutside { get; set; }
        public string ShipDate { get; set; }
        public string ShipTo { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToAddress3 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToName { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipViaType { get; set; }
        public string ShipViaTypeDescription { get; set; }
        public string ShippingInstructions { get; set; }
        public string SoldToAddress1 { get; set; }
        public string SoldToAddress2 { get; set; }
        public string SoldToAddress3 { get; set; }
        public string SoldToCity { get; set; }
        public string SoldToName { get; set; }
        public string SoldToState { get; set; }
        public string SoldToZipCode { get; set; }
        public string SpecialDiscountAmount { get; set; }
        public string StageCode { get; set; }
        public string StageCodeWord { get; set; }
        public string StagingArea { get; set; }
        public string StandingOrderDays { get; set; }
        public bool StandingOrderType { get; set; }
        public string StateCode { get; set; }
        public bool SubstituteFlag { get; set; }
        public string TakenBy { get; set; }
        public string TaxAmount1 { get; set; }
        public string TaxAmount2 { get; set; }
        public string TaxAmount3 { get; set; }
        public string TaxAmount4 { get; set; }
        public string TaxAuthority { get; set; }
        public string TaxDefaultType { get; set; }
        public string TaxOverrideCode { get; set; }
        public bool TaxOverrideFlag { get; set; }
        public bool TaxableFlag { get; set; }
        public string TenderAmount { get; set; }
        public string TermsDiscountAmount { get; set; }
        public string TermsPercent { get; set; }
        public string TermsType { get; set; }
        public string TermsTypeDescription { get; set; }
        public string TotalCostOrdered { get; set; }
        public string TotalCostShipped { get; set; }
        public string TotalCubes { get; set; }
        public string TotalInvoiceAmountOrdered { get; set; }
        public string TotalInvoiceAmountShipped { get; set; }
        public string TotalLineAmountOrdered { get; set; }
        public string TotalLineAmountShipped { get; set; }
        public string TotalQuantityOrdered { get; set; }
        public string TotalQuantityShipped { get; set; }
        public string TotalWeight { get; set; }
        public string TransactionType { get; set; }
        public bool UseTaxwareStepDataFlag { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public string User3 { get; set; }
        public string User4 { get; set; }
        public string User5 { get; set; }
        public string User6 { get; set; }
        public string User7 { get; set; }
        public string User8 { get; set; }
        public string User9 { get; set; }
        public string VendorRebateAmount { get; set; }
        public string Warehouse { get; set; }
        public string WholeOrderDiscountAmount { get; set; }
        public bool WholeOrderDiscountOverrideFlag { get; set; }
        public string WholeOrderDiscountPercent { get; set; }
        public bool WholeOrderDiscountType { get; set; }
    }

}
