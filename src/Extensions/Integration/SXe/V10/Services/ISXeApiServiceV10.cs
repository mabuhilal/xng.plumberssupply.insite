﻿namespace Insite.Integration.Connector.SXe.V10
{
    using Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.SFOEOrderTotLoadV4;

    internal interface IPlumbersSXeApiServiceV10 : IMultiInstanceDependency
    {
        SFOEOrderTotLoadV4Response SFOEOrderTotLoadV4(SFOEOrderTotLoadV4Request sfoeOrderTotLoadV4Request);
        OEOrderChangeResponse OEOrderChange(OEOrderChangeRequest sfoeOrderTotLoadV4Request);
        OEOrderChangeResponse OEOrderChangeRequestFinal(OEOrderChangeRequest orderChangeRequest);
    }
}