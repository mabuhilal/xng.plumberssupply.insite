﻿using System.IO;
using System.Net;
using System.Text;

using Insite.Common.Logging;
using Insite.Integration.Connector.SXe.V10.SXApi.Models.Shared;

using Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange;

namespace Extensions.Integration.SXe.V10.Services
{
    public class PlumbersOeOrderChangeService
    {
        protected CallConnection CallConnection { get; set; }
        protected OEOrderChangeRequest OEOrderChangeRequest { get; set; }
        public string SxUrl { get; set; }

        public PlumbersOeOrderChangeService(CallConnection callConnection, OEOrderChangeRequest oeOrderChangeRequest, string url)
        {
            CallConnection = callConnection;
            this.OEOrderChangeRequest = oeOrderChangeRequest;
            this.SxUrl = url;
        }

        public string CallOrderChangeApiEndpoint()
        {
            LogHelper.For(this).Debug($"URL: {this.SxUrl} Request: {this.OEOrderChangeRequest?.ToString()} Connection: {this.CallConnection?.ToString()}");
            var request = CreateSoapWebRequest();
            var soapString = CreateOeOrderChangeSoapString();
            LogHelper.For(this).Debug($"URL: {this.SxUrl} Stringified Request: {soapString} Connection: {this.CallConnection?.ToString()}");
            var bytes = Encoding.UTF8.GetBytes(soapString);
            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            LogHelper.For(this).Debug($"Request: {Encoding.UTF8.GetString(bytes)}");

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    string soapResult = rd.ReadToEnd();
                    return soapResult;
                }
            }
        }

        protected HttpWebRequest CreateSoapWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(SxUrl);
            webRequest.Headers.Add(@"SOAPAction:http://tempuri.org/IServiceOE/OEOrderChange");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        protected string CreateOeOrderChangeSoapString()
        {
            var headerString = BuildHeaderString();
            return $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"" xmlns:nxt=""http://schemas.datacontract.org/2004/07/NxT_API.com.infor.sxapi.connection"" xmlns:a=""http://schemas.datacontract.org/2004/07/NxT_API.com.infor.sxapi.OEOrderChange"">
               <soapenv:Header/>
               <soapenv:Body>
                  <tem:OEOrderChange>
                     <tem:callConnection>
                            <nxt:CompanyNumber>1</nxt:CompanyNumber>
                            <nxt:ConnectionString>{CallConnection.ConnectionString}</nxt:ConnectionString>
                            <nxt:OperatorInitials>{CallConnection.OperatorInitials}</nxt:OperatorInitials>
                            <nxt:OperatorPassword>{CallConnection.OperatorPassword}</nxt:OperatorPassword>
                           <nxt:StateFreeAppserver>false</nxt:StateFreeAppserver>
                     </tem:callConnection>
                     <tem:request>
        	            <a:IncludeLines>{OEOrderChangeRequest.IncludeLines.ToString().ToLower()}</a:IncludeLines>
        	            {headerString}
        	            <a:OrderNumber>{OEOrderChangeRequest.OrderNumber}</a:OrderNumber>
        	            <a:OrderSuffix>{OEOrderChangeRequest.OrderSuffix}</a:OrderSuffix>
                     </tem:request>
                  </tem:OEOrderChange>
               </soapenv:Body>
            </soapenv:Envelope>";
        }
        
        protected string BuildHeaderString()
        {
            var builder = new StringBuilder();
            builder.AppendLine(@"<a:Ininheader>");
            foreach (var headerData in OEOrderChangeRequest.Ininheader)
            {
                builder.AppendLine($@"<a:Ininheader>
                                   <a:ActualFreight>{headerData.ActualFreight}</a:ActualFreight>
                                    <a:AddonAmount1>{headerData.AddonAmount1}</a:AddonAmount1>
                                    <a:AddonAmount2>{headerData.AddonAmount2}</a:AddonAmount2>
                                    <a:AddonAmount3>{headerData.AddonAmount3}</a:AddonAmount3>
                                    <a:AddonAmount4>{headerData.AddonAmount4}</a:AddonAmount4>
                                    <a:AddonAmount5>{headerData.AddonAmount5}</a:AddonAmount5>
                                    <a:AddonAmount6>{headerData.AddonAmount6}</a:AddonAmount6>
                                    <a:AddonAmount7>{headerData.AddonAmount7}</a:AddonAmount7>
                                    <a:AddonAmount8>{headerData.AddonAmount8}</a:AddonAmount8>
                                    <a:AddonDescription1>{headerData.AddonDescription1}</a:AddonDescription1>
                                    <a:AddonDescription2>{headerData.AddonDescription2}</a:AddonDescription2>
                                    <a:AddonDescription3>{headerData.AddonDescription3}</a:AddonDescription3>
                                    <a:AddonDescription4>{headerData.AddonDescription4}</a:AddonDescription4>
                                    <a:AddonDescription5>{headerData.AddonDescription5}</a:AddonDescription5>
                                    <a:AddonDescription6>{headerData.AddonDescription6}</a:AddonDescription6>
                                    <a:AddonDescription7>{headerData.AddonDescription7}</a:AddonDescription7>
                                    <a:AddonDescription8>{headerData.AddonDescription8}</a:AddonDescription8>
                                    <a:AddonNetAmount1>{headerData.AddonNetAmount1}</a:AddonNetAmount1>
                                    <a:AddonNetAmount2>{headerData.AddonNetAmount2}</a:AddonNetAmount2>
                                    <a:AddonNetAmount3>{headerData.AddonNetAmount3}</a:AddonNetAmount3>
                                    <a:AddonNetAmount4>{headerData.AddonNetAmount4}</a:AddonNetAmount4>
                                    <a:AddonNetAmount5>{headerData.AddonNetAmount5}</a:AddonNetAmount5>
                                    <a:AddonNetAmount6>{headerData.AddonNetAmount6}</a:AddonNetAmount6>
                                    <a:AddonNetAmount7>{headerData.AddonNetAmount7}</a:AddonNetAmount7>
                                    <a:AddonNetAmount8>{headerData.AddonNetAmount8}</a:AddonNetAmount8>
                                    <a:AddonNumber1>{headerData.AddonNumber1}</a:AddonNumber1>
                                    <a:AddonNumber2>{headerData.AddonNumber2}</a:AddonNumber2>
                                    <a:AddonNumber3>{headerData.AddonNumber3}</a:AddonNumber3>
                                    <a:AddonNumber4>{headerData.AddonNumber4}</a:AddonNumber4>
                                    <a:AddonNumber5>{headerData.AddonNumber5}</a:AddonNumber5>
                                    <a:AddonNumber6>{headerData.AddonNumber6}</a:AddonNumber6>
                                    <a:AddonNumber7>{headerData.AddonNumber7}</a:AddonNumber7>
                                    <a:AddonNumber8>{headerData.AddonNumber8}</a:AddonNumber8>
                                    <a:AddonType1>{headerData.AddonType1}</a:AddonType1>
                                    <a:AddonType2>{headerData.AddonType2}</a:AddonType2>
                                    <a:AddonType3>{headerData.AddonType3}</a:AddonType3>
                                    <a:AddonType4>{headerData.AddonType4}</a:AddonType4>
                                    <a:AddonType5>{headerData.AddonType5}</a:AddonType5>
                                    <a:AddonType6>{headerData.AddonType6}</a:AddonType6>
                                    <a:AddonType7>{headerData.AddonType7}</a:AddonType7>
                                    <a:AddonType8>{headerData.AddonType8}</a:AddonType8>
                                    <a:ApprovalType>{headerData.ApprovalType}</a:ApprovalType>  
                                    <a:ApproveInitials>{headerData.ApproveInitials}</a:ApproveInitials>
                                    <a:BackorderExistsFlag>{headerData.BackorderExistsFlag.ToString().ToLower()}</a:BackorderExistsFlag>
                                    <a:BackorderFlag>{headerData.BackorderFlag.ToString().ToLower()}</a:BackorderFlag>
                                    <a:BackorderReleaseFlag>{headerData.BackorderReleaseFlag.ToString().ToLower()}</a:BackorderReleaseFlag>
                                    <a:BackorderStage>{headerData.BackorderStage}</a:BackorderStage>
                                    <a:BillDate>{headerData.BillDate}</a:BillDate>
                                    <a:CancelDate>{headerData.CancelDate}</a:CancelDate>
                                    <a:CityCode>{headerData.CityCode}</a:CityCode>
                                    <a:CodCollectedAmount>{headerData.CodCollectedAmount}</a:CodCollectedAmount>
                                    <a:CodFlag>{headerData.CodFlag.ToString().ToLower()}</a:CodFlag>
                                    <a:ContactID>{headerData.ContactID}</a:ContactID>
                                    <a:ContactName>{headerData.ContactName}</a:ContactName>
                                    <a:CountryCode>{headerData.CountryCode}</a:CountryCode>
                                    <a:CountyCode>{headerData.CountyCode}</a:CountyCode>
                                    <a:CreditMemoReasonType>{headerData.CreditMemoReasonType}</a:CreditMemoReasonType>
                                    <a:CustomerAddress1>{headerData.CustomerAddress1}</a:CustomerAddress1>
                                    <a:CustomerAddress2>{headerData.CustomerAddress2}</a:CustomerAddress2>
                                    <a:CustomerAddress3>{headerData.CustomerAddress3}</a:CustomerAddress3>
                                    <a:CustomerCity>{headerData.CustomerCity}</a:CustomerCity>
                                    <a:CustomerName>{headerData.CustomerName}</a:CustomerName>
                                    <a:CustomerNumber>{headerData.CustomerNumber}</a:CustomerNumber>
                                    <a:CustomerPhoneNumber>{headerData.CustomerPhoneNumber}</a:CustomerPhoneNumber>
                                    <a:CustomerPurchaseOrder>{headerData.CustomerPurchaseOrder}</a:CustomerPurchaseOrder>
                                    <a:CustomerState>{headerData.CustomerState}</a:CustomerState>
                                    <a:CustomerZipCode>{headerData.CustomerZipCode}</a:CustomerZipCode>
                                    <a:DatcOverrideFlag>{headerData.DatcOverrideFlag.ToString().ToLower()}</a:DatcOverrideFlag>
                                    <a:DirectRouteDeliveryDate>{headerData.DirectRouteDeliveryDate}</a:DirectRouteDeliveryDate>
                                    <a:DirectRouteDeliveryTime>{headerData.DirectRouteDeliveryTime}</a:DirectRouteDeliveryTime>
                                    <a:DirectRouteExportFlag>{headerData.DirectRouteExportFlag.ToString().ToLower()}</a:DirectRouteExportFlag>
                                    <a:DirectRouteHoldFlag>{headerData.DirectRouteHoldFlag.ToString().ToLower()}</a:DirectRouteHoldFlag>
                                    <a:DownPaymentAmount>{headerData.DownPaymentAmount}</a:DownPaymentAmount>
                                    <a:EnterDate>{headerData.EnterDate}</a:EnterDate>
                                    <a:Filler1>{headerData.Filler1}</a:Filler1>
                                    <a:Filler2>{headerData.Filler2}</a:Filler2>
                                    <a:Filler3>{headerData.Filler3}</a:Filler3>
                                    <a:FloorPlanCustomerNumber>{headerData.FloorPlanCustomerNumber}</a:FloorPlanCustomerNumber>
                                    <a:InBoundFreightFlag>{headerData.InBoundFreightFlag.ToString().ToLower()}</a:InBoundFreightFlag>
                                    <a:InvoiceDate>{headerData.InvoiceDate}</a:InvoiceDate>
                                    <a:InvoiceNumber>{headerData.InvoiceNumber}</a:InvoiceNumber>
                                    <a:InvoiceSuffix>{headerData.InvoiceSuffix}</a:InvoiceSuffix>
                                    <a:JobNumber>{headerData.JobNumber}</a:JobNumber>
                                    <a:JrnlNumber2>{headerData.JrnlNumber2}</a:JrnlNumber2>
                                    <a:LanguageCode>{headerData.LanguageCode}</a:LanguageCode>
                                    <a:LastUpdate>{headerData.LastUpdate}</a:LastUpdate>
                                    <a:LineFlag>{headerData.LineFlag.ToString().ToLower()}</a:LineFlag>
                                    <a:LockFlag>{headerData.LockFlag.ToString().ToLower()}</a:LockFlag>
                                    <a:LostBusinessType>{headerData.LostBusinessType}</a:LostBusinessType>
                                    <a:LumpBillingAmount>{headerData.LumpBillingAmount}</a:LumpBillingAmount>
                                    <a:LumpBillingFlag>{headerData.LumpBillingFlag.ToString().ToLower()}</a:LumpBillingFlag>
                                    <a:LumpPriceFlag>{headerData.LumpPriceFlag.ToString().ToLower()}</a:LumpPriceFlag>
                                    <a:NextLineNumber>{headerData.NextLineNumber}</a:NextLineNumber>
                                    <a:NonTaxType>{headerData.NonTaxType}</a:NonTaxType>
                                    <a:NumberDirectOrderLines>{headerData.NumberDirectOrderLines}</a:NumberDirectOrderLines>
                                    <a:NumberLineItems>{headerData.NumberLineItems}</a:NumberLineItems>
                                    <a:NumberPackages>{headerData.NumberPackages}</a:NumberPackages>
                                    <a:OrderDisposition>{headerData.OrderDisposition}</a:OrderDisposition>
                                    <a:OrderDispositionWord>{headerData.OrderDispositionWord}</a:OrderDispositionWord>
                                    <a:Other1Code>{headerData.Other1Code}</a:Other1Code>
                                    <a:Other2Code>{headerData.Other2Code}</a:Other2Code>
                                    <a:OutBoundFreightFlag>{headerData.OutBoundFreightFlag.ToString().ToLower()}</a:OutBoundFreightFlag>
                                    <a:PackageID>{headerData.PackageID}</a:PackageID>
                                    <a:PaidDate>{headerData.PaidDate}</a:PaidDate>
                                    <a:PickCount>{headerData.PickCount}</a:PickCount>
                                    <a:PickPrintFlag>{headerData.PickPrintFlag.ToString().ToLower()}</a:PickPrintFlag>
                                    <a:PickedDate>{headerData.PickedDate}</a:PickedDate>
                                    <a:PickedInitials>{headerData.PickedInitials}</a:PickedInitials>                                  
                                    <a:PlacedBy>{headerData.PlacedBy}</a:PlacedBy>   
                                    <a:PmFlag>{headerData.PmFlag.ToString().ToLower()}</a:PmFlag>
                                    <a:PriceCode>{headerData.PriceCode}</a:PriceCode>
                                    <a:PromiseDate>{headerData.PromiseDate}</a:PromiseDate>
                                    <a:ProposalNumber>{headerData.ProposalNumber}</a:ProposalNumber>
                                    <a:ProspectNumber>{headerData.ProspectNumber}</a:ProspectNumber>
                                    <a:PstLicenseNumber>{headerData.PstLicenseNumber}</a:PstLicenseNumber>
                                    <a:PurchasingAgentName>{headerData.PurchasingAgentName}</a:PurchasingAgentName>
                                    <a:PurchasingAgentPhoneNumber>{headerData.PurchasingAgentPhoneNumber}</a:PurchasingAgentPhoneNumber>
                                    <a:Reference>{headerData.Reference}</a:Reference>
                                    <a:RequestedShipDate>{headerData.RequestedShipDate}</a:RequestedShipDate>
                                    <a:Route>{headerData.Route}</a:Route>
                                    <a:SalesRepInside>{headerData.SalesRepInside}</a:SalesRepInside>
                                    <a:SalesRepOutside>{headerData.SalesRepOutside}</a:SalesRepOutside>
                                    <a:ShipDate>{headerData.ShipDate}</a:ShipDate>
                                    <a:ShipTo>{headerData.ShipTo}</a:ShipTo>
                                    <a:ShipToAddress1>{headerData.ShipToAddress1}</a:ShipToAddress1>
                                    <a:ShipToAddress2>{headerData.ShipToAddress2}</a:ShipToAddress2>
                                    <a:ShipToAddress3>{headerData.ShipToAddress3}</a:ShipToAddress3>
                                    <a:ShipToCity>{headerData.ShipToCity}</a:ShipToCity>
                                    <a:ShipToName>{headerData.ShipToName}</a:ShipToName>
                                    <a:ShipToState>{headerData.ShipToState}</a:ShipToState>
                                    <a:ShipToZip>{headerData.ShipToZip}</a:ShipToZip>
                                    <a:ShipViaType>{headerData.ShipViaType}</a:ShipViaType>
                                    <a:ShipViaTypeDescription>{headerData.ShipViaTypeDescription}</a:ShipViaTypeDescription>
                                    <a:ShippingInstructions>{headerData.ShippingInstructions}</a:ShippingInstructions>
                                    <a:SoldToAddress1>{headerData.SoldToAddress1}</a:SoldToAddress1>
                                    <a:SoldToAddress2>{headerData.SoldToAddress2}</a:SoldToAddress2>
                                    <a:SoldToAddress3>{headerData.SoldToAddress3}</a:SoldToAddress3>
                                    <a:SoldToCity>{headerData.SoldToCity}</a:SoldToCity>
                                    <a:SoldToName>{headerData.SoldToName}</a:SoldToName>
                                    <a:SoldToState>{headerData.SoldToState}</a:SoldToState>
                                    <a:SoldToZipCode>{headerData.SoldToZipCode}</a:SoldToZipCode>
                                    <a:SpecialDiscountAmount>{headerData.SpecialDiscountAmount}</a:SpecialDiscountAmount>
                                    <a:StageCode>{headerData.StageCode}</a:StageCode>
                                    <a:StageCodeWord>{headerData.StageCodeWord}</a:StageCodeWord>
                                    <a:StagingArea>{headerData.StagingArea}</a:StagingArea>
                                    <a:StandingOrderDays>{headerData.StandingOrderDays}</a:StandingOrderDays>
                                    <a:StandingOrderType>{headerData.StandingOrderType.ToString().ToLower()}</a:StandingOrderType>
                                    <a:StateCode>{headerData.StateCode}</a:StateCode>
                                    <a:SubstituteFlag>{headerData.SubstituteFlag.ToString().ToLower()}</a:SubstituteFlag>
                                    <a:TakenBy>{headerData.TakenBy}</a:TakenBy>
                                    <a:TaxAmount1>{headerData.TaxAmount1}</a:TaxAmount1>
                                    <a:TaxAmount2>{headerData.TaxAmount2}</a:TaxAmount2>
                                    <a:TaxAmount3>{headerData.TaxAmount3}</a:TaxAmount3>
                                    <a:TaxAmount4>{headerData.TaxAmount4}</a:TaxAmount4>
                                    <a:TaxAuthority>{headerData.TaxAuthority}</a:TaxAuthority>
                                    <a:TaxDefaultType>{headerData.TaxDefaultType}</a:TaxDefaultType>
                                    <a:TaxOverrideCode>{headerData.TaxOverrideCode}</a:TaxOverrideCode>
                                    <a:TaxOverrideFlag>{headerData.TaxOverrideFlag.ToString().ToLower()}</a:TaxOverrideFlag>
                                    <a:TaxableFlag>{headerData.TaxableFlag.ToString().ToLower()}</a:TaxableFlag>
                                    <a:TenderAmount>{headerData.TenderAmount}</a:TenderAmount>
                                    <a:TermsDiscountAmount>{headerData.TermsDiscountAmount}</a:TermsDiscountAmount>
                                    <a:TermsPercent>{headerData.TermsPercent}</a:TermsPercent>
                                    <a:TermsType>{headerData.TermsType}</a:TermsType>
                                    <a:TermsTypeDescription>{headerData.TermsTypeDescription}</a:TermsTypeDescription>
                                    <a:TotalCostOrdered>{headerData.TotalCostOrdered}</a:TotalCostOrdered>
                                    <a:TotalCostShipped>{headerData.TotalCostShipped}</a:TotalCostShipped>
                                    <a:TotalCubes>{headerData.TotalCubes}</a:TotalCubes>
                                    <a:TotalInvoiceAmountOrdered>{headerData.TotalInvoiceAmountOrdered}</a:TotalInvoiceAmountOrdered>
                                    <a:TotalInvoiceAmountShipped>{headerData.TotalInvoiceAmountShipped}</a:TotalInvoiceAmountShipped>
                                    <a:TotalLineAmountOrdered>{headerData.TotalLineAmountOrdered}</a:TotalLineAmountOrdered>
                                    <a:TotalLineAmountShipped>{headerData.TotalLineAmountShipped}</a:TotalLineAmountShipped>
                                    <a:TotalQuantityOrdered>{headerData.TotalQuantityOrdered}</a:TotalQuantityOrdered>
                                    <a:TotalQuantityShipped>{headerData.TotalQuantityShipped}</a:TotalQuantityShipped>
                                    <a:TotalWeight>{headerData.TotalWeight}</a:TotalWeight>
                                    <a:TransactionType>{headerData.TransactionType}</a:TransactionType>
                                    <a:UseTaxwareStepDataFlag>{headerData.UseTaxwareStepDataFlag.ToString().ToLower()}</a:UseTaxwareStepDataFlag>
                                    <a:User1>{headerData.User1}</a:User1>
                                    <a:User2>{headerData.User2}</a:User2>
                                    <a:User3>{headerData.User3}</a:User3>
                                    <a:User4>{headerData.User4}</a:User4>
                                    <a:User5>{headerData.User5}</a:User5>
                                    <a:User6>{headerData.User6}</a:User6>
                                    <a:User7>{headerData.User7}</a:User7>
                                    <a:User8>{headerData.User8}</a:User8>
                                    <a:User9>{headerData.User9}</a:User9>
                                    <a:VendorRebateAmount>{headerData.VendorRebateAmount}</a:VendorRebateAmount>
                                    <a:Warehouse>{headerData.Warehouse}</a:Warehouse>
                                    <a:WholeOrderDiscountAmount>{headerData.WholeOrderDiscountAmount}</a:WholeOrderDiscountAmount>
                                    <a:WholeOrderDiscountOverrideFlag>{headerData.WholeOrderDiscountOverrideFlag.ToString().ToLower()}</a:WholeOrderDiscountOverrideFlag>
                                    <a:WholeOrderDiscountPercent>{headerData.WholeOrderDiscountPercent}</a:WholeOrderDiscountPercent>
                                    <a:WholeOrderDiscountType>{headerData.WholeOrderDiscountType.ToString().ToLower()}</a:WholeOrderDiscountType>
                                    </a:Ininheader>");
            }
            builder.AppendLine(@"</a:Ininheader>");
            return builder.ToString();
        }
    }
}