﻿namespace Extensions.Integration.SXe.V10.Services
{
    using System;

    using Insite.Common.Dependencies;
    using Insite.Common.Helpers;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.SystemSetting.Groups.Integration;
    using Insite.Data.Entities;
    using Insite.Integration.Connector.SXe.V10;

    internal sealed class PlumbersSXeApiServiceFactory : IPlumbersSXeApiServiceFactory
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        private readonly IDependencyLocator dependencyLocator;

        private readonly IntegrationConnectorSettings integrationConnectorSettings;

        public PlumbersSXeApiServiceFactory(IUnitOfWorkFactory unitOfWorkFactory, IDependencyLocator dependencyLocator, IntegrationConnectorSettings integrationConnectorSettings)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.dependencyLocator = dependencyLocator;
            this.integrationConnectorSettings = integrationConnectorSettings;
        }

        public IPlumbersSXeApiServiceV10 GetSXeApiServiceV10(IntegrationConnection integrationConnection = null)
        {
            integrationConnection = this.GetIntegrationConnection(integrationConnection);

            var companyNumber = GetCompanyNumber(integrationConnection);
            var operatorPassword = GetOperatorPassword(integrationConnection);

            return new PlumbersSXeApiServiceV10(integrationConnection.Url, integrationConnection.AppServerHost, integrationConnection.LogOn, operatorPassword, companyNumber);
        }

        private IntegrationConnection GetIntegrationConnection(IntegrationConnection integrationConnection)
        {
            if (integrationConnection != null)
            {
                return integrationConnection;
            }

            var sXeIntegrationConnectionId = this.dependencyLocator.GetInstance<IntegrationConnectorSettings>().SXeIntegrationConnectionId;
            if (sXeIntegrationConnectionId.IsBlank())
            {
                throw new ArgumentException("SXe Integration Connector requires an Integration Connection.");
            }

            integrationConnection = this.unitOfWorkFactory.GetUnitOfWork().GetRepository<IntegrationConnection>().Get(sXeIntegrationConnectionId);
            if (integrationConnection == null)
            {
                throw new ArgumentException("SXe Integration Connector requires an Integration Connection.");
            }

            return integrationConnection;
        }

        private static int GetCompanyNumber(IntegrationConnection integrationConnection)
        {
            int companyNumber;
            if (!int.TryParse(integrationConnection.SystemNumber, out companyNumber))
            {
                throw new ArgumentException("SXe Integration Connection company number must be in integer format.");
            }

            return companyNumber;
        }

        private static string GetOperatorPassword(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.Password);
        }

        private static string GetClientSecret(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.MessageServerService);
        }

        private static string GetPassword(IntegrationConnection integrationConnection)
        {
            return EncryptionHelper.DecryptAes(integrationConnection.GatewayService);
        }
    }
}