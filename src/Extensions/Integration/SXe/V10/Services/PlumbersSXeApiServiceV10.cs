﻿namespace Insite.Integration.Connector.SXe.V10
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange;
    using Extensions.Integration.SXe.V10.Services;
    using Insite.Common.Logging;
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.SFOEOrderTotLoadV4;
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.Shared;

    internal sealed class PlumbersSXeApiServiceV10 : IPlumbersSXeApiServiceV10
    {
        private const string SxApiPath = "sxapi";

        private const string ServiceARApiPath = "ServiceAR.svc";

        private const string ServiceOEApiPath = "ServiceOE.svc";

        private const string ServiceSFApiPath = "ServiceSF.svc";

        private readonly string url;

        private readonly string connectionString;

        private readonly string operatorInitials;

        private readonly string operatorPassword;

        private readonly int companyNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlumbersSXeApiServiceV10"/> class.
        /// </summary>
        /// <param name="url">The url of the SXe web service.</param>
        /// <param name="connectionString">The connection string of the app server used to populate the <see cref="CallConnection"/>.</param>
        /// <param name="operatorInitials">The operator initials used to populate the <see cref="CallConnection"/>.</param>
        /// <param name="operatorPassword">The operator password used to populate the <see cref="CallConnection"/>.</param>
        /// <param name="companyNumber">The company number used to populate the <see cref="CallConnection"/>.</param>
        public PlumbersSXeApiServiceV10(string url, string connectionString, string operatorInitials, string operatorPassword, int companyNumber)
        {
            this.url = url;
            this.connectionString = connectionString;
            this.operatorInitials = operatorInitials;
            this.operatorPassword = operatorPassword;
            this.companyNumber = companyNumber;
        }

        /// <summary>
        /// Executes the SFOEOrderTotLoadV4 with the provided <see cref="SFOEOrderTotLoadV4Request"/>
        /// </summary>
        /// <param name="sfoeOrderTotLoadV4Request">The <see cref="SFOEOrderTotLoadV4Request"/>.</param>
        /// <returns>The <see cref="SFOEOrderTotLoadV4Response"/>.</returns>
        public SFOEOrderTotLoadV4Response SFOEOrderTotLoadV4(SFOEOrderTotLoadV4Request sfoeOrderTotLoadV4Request)
        {
            LogHelper.For(this).Debug($"{nameof(SFOEOrderTotLoadV4Request)}: {GetSerializedValue(sfoeOrderTotLoadV4Request)}");

            SFOEOrderTotLoadV4Response sfOEOrderTotLoadV4Response = null;

            try
            {
                var sXeSoapService = CreateSXeSoapService(this.url, SxApiPath, ServiceSFApiPath);
                var callConnection = CreateCallConnection(this.companyNumber, this.connectionString, this.operatorInitials, this.operatorPassword);

                sfOEOrderTotLoadV4Response = sXeSoapService.SFOEOrderTotLoadV4(callConnection, sfoeOrderTotLoadV4Request);
            }
            catch (Exception exception)
            {
                sfOEOrderTotLoadV4Response = new SFOEOrderTotLoadV4Response { ErrorMessage = exception.Message };
            }

            LogHelper.For(this).Debug($"{nameof(SFOEOrderTotLoadV4Response)}: {GetSerializedValue(sfOEOrderTotLoadV4Response)}");

            return sfOEOrderTotLoadV4Response;
        }

        public OEOrderChangeResponse OEOrderChange(OEOrderChangeRequest oeOrderChangeRequest)
        {
            LogHelper.For(this).Debug($"{nameof(OEOrderChangeRequest)}: {GetSerializedValue(oeOrderChangeRequest)}");

            OEOrderChangeResponse oeOrderChangeResponse = null;

            try
            {                
                var sXeSoapService = CreateSXeSoapService(this.url, SxApiPath, ServiceOEApiPath);
                var callConnection = CreateCallConnection(this.companyNumber, this.connectionString, this.operatorInitials, this.operatorPassword);

                oeOrderChangeResponse = sXeSoapService.OEOrderChange(callConnection, oeOrderChangeRequest);
            }
            catch (Exception exception)
            {
                oeOrderChangeResponse = new OEOrderChangeResponse { ErrorMessage = exception.Message };
            }

            LogHelper.For(this).Debug($"{nameof(OEOrderChangeResponse)}: {GetSerializedValue(oeOrderChangeResponse)}");

            return oeOrderChangeResponse;
        }

        public OEOrderChangeResponse OEOrderChangeRequestFinal(OEOrderChangeRequest orderChangeRequest)
        {
            LogHelper.For(this).Debug($"{nameof(OEOrderChangeRequest)}: {GetSerializedValue(orderChangeRequest)}");

            string orderChangeResponse = null;

            try
            {
                var callConnection = CreateCallConnection(this.companyNumber, this.connectionString, this.operatorInitials, this.operatorPassword);
                var sxOeUrl = CreateUrl(this.url, SxApiPath, ServiceOEApiPath);
                var service = new PlumbersOeOrderChangeService(callConnection, orderChangeRequest, sxOeUrl);
                orderChangeResponse = service.CallOrderChangeApiEndpoint();
            }
            catch (Exception exception)
            {
                return new OEOrderChangeResponse() { ErrorMessage = exception.Message };
            }

            LogHelper.For(this).Debug($"{nameof(OEOrderChangeResponse)}: Call succeeded: {orderChangeResponse}"); ;

            return new OEOrderChangeResponse();
        }

        private static PlumbersSXeSoapServiceV10 CreateSXeSoapService(string baseUrl, params string[] apiPaths)
        {
            return new PlumbersSXeSoapServiceV10(CreateUrl(baseUrl, apiPaths));
        }

        private static CallConnection CreateCallConnection(int companyNumber, string connectionString, string operatorInitials, string operatorPassword)
        {
            return new CallConnection
            {
                CompanyNumber = companyNumber,
                ConnectionString = connectionString,
                OperatorInitials = operatorInitials,
                OperatorPassword = operatorPassword,
                StateFreeAppserver = false
            };
        }

        private static string CreateUrl(string baseUrl, params string[] apiPaths)
        {
            var url = string.Empty;

            baseUrl = baseUrl.TrimEnd('/');

            foreach (var apiPath in apiPaths.Reverse())
            {
                if (baseUrl.EndsWith(apiPath, StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }

                url = $"{apiPath}/{url}";
            }

            url = $"{baseUrl}/{url}";

            return url.TrimEnd('/');
        }

        public static string GetSerializedValue(object value)
        {
            var serializer = new XmlSerializer(value.GetType());
            var writer = new StringWriter();

            serializer.Serialize(writer, value);

            return writer.ToString();
        }
    }
}