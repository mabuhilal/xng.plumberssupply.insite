﻿namespace Extensions.Integration.SXe.V10.Services
{
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.SFOEOrderTotLoadV4;
    using Insite.Integration.Connector.SXe.V10.SXApi.Models.Shared;
    
    using Extensions.Integration.SXe.V10.IonApi.Models.OEOrderChange;

    [System.Web.Services.WebServiceBindingAttribute]
    public class PlumbersSXeSoapServiceV10 : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public PlumbersSXeSoapServiceV10(string url)
        {
            this.Url = url;
        }

        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IServiceSF/SFOEOrderTotLoadV4", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public SFOEOrderTotLoadV4Response SFOEOrderTotLoadV4([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] CallConnection callConnection, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] SFOEOrderTotLoadV4Request request)
        {
            var results = this.Invoke("SFOEOrderTotLoadV4", new object[] { callConnection, request });
            return (SFOEOrderTotLoadV4Response)results[0];
        }

        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IServiceOE/OEOrderChange", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public OEOrderChangeResponse OEOrderChange(
            [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] CallConnection callConnection, 
            [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] OEOrderChangeRequest request)
        {
            object[] results = this.Invoke("OEOrderChange", new object[] {
                callConnection,
                request});
            return ((OEOrderChangeResponse)(results[0]));
        }
    }
}