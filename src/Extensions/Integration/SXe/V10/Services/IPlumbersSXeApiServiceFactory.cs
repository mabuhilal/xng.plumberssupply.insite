﻿namespace Extensions.Integration.SXe.V10.Services
{
    using Insite.Core.Interfaces.Dependency;
    using Insite.Data.Entities;
    using Insite.Integration.Connector.SXe.V10;

    /// <summary>The SXe Api Service Factory Interface.</summary>
    internal interface IPlumbersSXeApiServiceFactory : ISingletonLifetime
    {
        IPlumbersSXeApiServiceV10 GetSXeApiServiceV10(IntegrationConnection integrationConnection = null);
    }
}