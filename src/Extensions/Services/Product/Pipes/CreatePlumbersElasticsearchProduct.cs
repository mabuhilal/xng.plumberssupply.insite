﻿using System;
using System.Collections.Generic;
using System.Linq;

using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Pipelines;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index.Pipelines.Parameters;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index.Pipelines.Results;

using Extensions.Models;

namespace Extensions.Services.Product.Pipes
{
    public class CreatePlumbersElasticsearchProduct : IPipe<CreateElasticsearchProductParameter, CreateElasticsearchProductResult>
    {
        public int Order => 151;

        public CreateElasticsearchProductResult Execute(IUnitOfWork unitOfWork, CreateElasticsearchProductParameter parameter, CreateElasticsearchProductResult result)
        {
            var plumbersElasticsearchProduct = new PlumbersElasticsearchProduct(result.ElasticsearchProduct);

            if (!plumbersElasticsearchProduct.CustomProperties.ContainsKey(PlumbersConstants.ProductModelNumbersKey))
            {
                return result;
            }

            plumbersElasticsearchProduct.ModelNumbers = this.ExtractList(plumbersElasticsearchProduct?.CustomProperties[PlumbersConstants.ProductModelNumbersKey])
                .Distinct()
                .ToList();

            result.ElasticsearchProduct = plumbersElasticsearchProduct;
            
            return result;
        }

        private List<string> ExtractList(string content)
        {
            return content?.Split(new[] { Convert.ToChar(255) }, StringSplitOptions.RemoveEmptyEntries)
                .Select(o => o.Replace(",", string.Empty))
                .ToList() ?? new List<string>();
        }
    }
}