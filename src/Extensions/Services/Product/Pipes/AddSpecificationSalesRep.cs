﻿using System.Linq;

using Insite.Catalog.Services.Pipelines.Parameters;
using Insite.Catalog.Services.Pipelines.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Pipelines;

namespace Extensions.Services.Product.Pipes
{
    public sealed class AddSpecificationSalesRep : IPipe<CreateProductDtoParameter, CreateProductDtoResult>
    {
        public int Order => 1901;

        public CreateProductDtoResult Execute(IUnitOfWork unitOfWork, CreateProductDtoParameter parameter, CreateProductDtoResult result)
        {
            // Remove specifications from guest sessions that contain a sales rep's only specification custom property. 
            if (SiteContext.Current.UserProfileDto == null)
            {
                foreach (var specification in parameter.Product.Specifications.Where(o => o.IsActive))
                {
                    var specificationOnlyVisibleToSalesReps = specification.CustomProperties
                        .FirstOrDefault(o => o.Name == Extensions.PlumbersConstants.SpecificationVisibleOnlyForSalesRepsKey);

                    // If current specification does not have custom property visiableOnlyToSalesRep, leave it in the specifications collection.
                    if (specificationOnlyVisibleToSalesReps == null)
                    {
                        continue;
                    }

                    bool.TryParse(specificationOnlyVisibleToSalesReps.Value, out bool isVisibleToOnlySalesRep);

                    if (isVisibleToOnlySalesRep == false)
                    {
                        continue;
                    }
                    else
                    {
                        result.ProductDto.Specifications
                            .Remove(result.ProductDto.Specifications
                                .FirstOrDefault(
                                    o => o.Name == specification.Name
                                    && o.Value == specification.Value));
                    }
                }
            }

            if (!parameter.GetSpecifications || SiteContext.Current.BillTo == null)
            {
                return result;
            }

            // If user is a sales person, remove specifications that have specification custom prop salesRepNumber that do not contain current reps salesRepNumber. 
            if (SiteContext.Current.UserProfileDto != null
                && SiteContext.Current.BillTo != null)
            {
                foreach (var specification in parameter.Product.Specifications.Where(o => o.IsActive))
                {
                    var specificationOnlyVisibleToSalesReps = specification.CustomProperties
                        .FirstOrDefault(o => o.Name == Extensions.PlumbersConstants.SpecificationVisibleOnlyForSalesRepsKey);

                    bool.TryParse(specificationOnlyVisibleToSalesReps?.Value, out bool isVisibleToOnlySalesRep);

                    if (specificationOnlyVisibleToSalesReps == null
                        || isVisibleToOnlySalesRep == false)
                    {
                        continue;
                    }
                    else
                    {
                        // Remove from specifications collection if the current sales rep number does not match the current specification custom prop value.
                        var primarySalesRep = SiteContext.Current.UserProfileDto.Salespersons
                            .FirstOrDefault(o => o.Id == SiteContext.Current.BillTo.PrimarySalespersonId);

                        if (primarySalesRep == null)
                        {
                            if (isVisibleToOnlySalesRep)
                            {
                                result.ProductDto.Specifications
                                    .Remove(result.ProductDto.Specifications
                                        .FirstOrDefault(
                                            o => o.Name == specification.Name 
                                            && o.Value == specification.Value));
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}