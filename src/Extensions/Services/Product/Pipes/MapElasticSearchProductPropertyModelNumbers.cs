﻿using System;

using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Pipelines;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index.Pipelines.Parameters;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index.Pipelines.Results;

using Nest;

namespace Extensions.Services.Product.Pipes
{
    public class MapElasticSearchProductPropertyModelNumbers : IPipe<MapElasticSearchProductPropertyParameter, MapElasticSearchProductPropertyResult>
    {
        public int Order => 200;

        public MapElasticSearchProductPropertyResult Execute(IUnitOfWork unitOfWork, MapElasticSearchProductPropertyParameter parameter, MapElasticSearchProductPropertyResult result)
        {
            if (parameter.PropertyDefinition != null
                && parameter.PropertyDefinition.IsCustomProperty
                && parameter.PropertyDefinition.Name.EqualsIgnoreCase(PlumbersConstants.ProductModelNumbersKey))
            {
                result.Property = new TextProperty();
            }

            return result;
        }
    }
}