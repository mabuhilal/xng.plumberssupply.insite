﻿namespace Extensions.Services.Product.Pipes
{
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Search.Elasticsearch.DocumentTypes.Product.Query.Pipelines;
    using Insite.Search.Elasticsearch.DocumentTypes.Product.Query.Pipelines.Parameters;
    using Insite.Search.Elasticsearch.DocumentTypes.Product.Query.Pipelines.Results;

    public sealed class LimitProductWarehouseSearchResults : IPipe<RunProductSearchParameter, RunProductSearchResult>
    {
        private readonly IProductSearchPipeline productSearchPipeline;

        public LimitProductWarehouseSearchResults(IProductSearchPipeline productSearchPipeline)
        {
            this.productSearchPipeline = productSearchPipeline;
        }

        public int Order => 999;

        public RunProductSearchResult Execute(IUnitOfWork unitOfWork, RunProductSearchParameter parameter, RunProductSearchResult result)
        {
            result.FormProductFilterResult = this.productSearchPipeline.FormProductFilter(new FormProductFilterParameter(parameter.SiteContext, parameter.ProductSearchParameter)
            {
                ElasticsearchQueryBuilder = result.ElasticsearchQueryBuilder,
                IncludeProductsWithoutCategory = parameter.ProductSearchParameter?.IncludeProductsWithoutCategory ?? false
            });

            return result;
        }
    }
}
