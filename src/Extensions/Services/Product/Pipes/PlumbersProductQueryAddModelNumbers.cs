﻿namespace Extensions.Services.Product.Pipes
{
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Search.Elasticsearch.DocumentTypes.Product.Query.Pipelines.Parameters;
    using Insite.Search.Elasticsearch.DocumentTypes.Product.Query.Pipelines.Results;

    public sealed class PlumbersProductQueryAddModelNumbers : IPipe<RunProductSearchParameter, RunProductSearchResult>
    {
        public int Order => 501;
        
        public RunProductSearchResult Execute(IUnitOfWork unitOfWork, RunProductSearchParameter parameter, RunProductSearchResult result)
        {
            result.PhraseMatchFields.Add(PlumbersConstants.ProductModelNumbersKey);
            return result;
        }
    }
}