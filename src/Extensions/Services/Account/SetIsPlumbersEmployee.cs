﻿namespace Extensions.Services.Account
{
    using Insite.Account.Services.Parameters;
    using Insite.Account.Services.Results;
    using Insite.Core.Context;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services.Handlers;

    [DependencyName(nameof(SetIsPlumbersEmployee))]
    public sealed class SetIsPlumbersEmployee : HandlerBase<GetSessionParameter, GetSessionResult>
    {
        public override int Order => 912;

        public override GetSessionResult Execute(IUnitOfWork unitOfWork, GetSessionParameter parameter, GetSessionResult result)
        {
            if (SiteContext.Current.UserProfile != null)
            {
                var isPlumbersEmployee = SiteContext.Current.UserProfile.GetProperty(PlumbersConstants.PlumbersEmployeeKey, "false");
                result.Properties[PlumbersConstants.PlumbersEmployeeKey] = isPlumbersEmployee;
            }

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}