﻿namespace Extensions.Services.Session.Handlers
{
    using System;
    using Extensions.Services.BillTrust.Sso;
    using Insite.Account.Services;
    using Insite.Account.Services.Parameters;
    using Insite.Account.Services.Pipelines;
    using Insite.Account.Services.Results;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services.Handlers;

    [DependencyName(nameof(PlumbersAddBillTrustToSession))]
    public sealed class PlumbersAddBillTrustToSession : HandlerBase<GetSessionParameter, GetSessionResult>
    {
        private readonly Lazy<IAccountService> accountService;
        private readonly Lazy<IAccountPipeline> accountPipeline;
        private readonly Lazy<PlumbersBillTrustProvider> billTrustProvider;

        public PlumbersAddBillTrustToSession(Lazy<IAccountService> accountService, 
            Lazy<IAccountPipeline> accountPipeline,
            Lazy<PlumbersBillTrustProvider> billTrustProvider)
        {
            this.accountService = accountService;
            this.accountPipeline = accountPipeline;
            this.billTrustProvider = billTrustProvider;
        }

        public override int Order => 701;

        public override GetSessionResult Execute(IUnitOfWork unitOfWork, GetSessionParameter parameter, GetSessionResult result)
        {
            if (!result.IsAuthenticated) 
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            result.Properties.Add(PlumbersConstants.BillTrustUrlKey, this.billTrustProvider.Value?.GetBillTrustInvoiceUrl() ?? string.Empty);
            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}