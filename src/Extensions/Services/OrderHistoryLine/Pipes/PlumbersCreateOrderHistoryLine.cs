﻿namespace Extensions.Services.OrderHistoryLine.Pipes
{
    using Insite.Cart.Services.Pipelines.Parameters;
    using Insite.Cart.Services.Pipelines.Results;
    using Insite.Common.Helpers;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Plugins.EntityUtilities;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Core.Plugins.Utilities;
    using Insite.Data.Entities;

    public sealed class PlumbersCreateOrderHistoryLine : IPipe<CreateOrderHistoryLineParameter, CreateOrderHistoryLineResult>
    {
        private readonly IObjectToObjectMapper objectToObjectMapper;

        private readonly IOrderLineUtilities orderLineUtilities;

        public int Order => 200;

        public PlumbersCreateOrderHistoryLine(IObjectToObjectMapper objectToObjectMapper, IOrderLineUtilities orderLineUtilities)
        {
            this.objectToObjectMapper = objectToObjectMapper;
            this.orderLineUtilities = orderLineUtilities;
        }

        public CreateOrderHistoryLineResult Execute(IUnitOfWork unitOfWork, CreateOrderHistoryLineParameter parameter, CreateOrderHistoryLineResult result)
        {
            var orderLine = parameter.OrderLine;
            result.OrderHistoryLine.UnitListPrice = NumberHelper.RoundCurrency(orderLine.Product?.BasicListPrice ?? 0);
            
            return result;
        }
    }
}
