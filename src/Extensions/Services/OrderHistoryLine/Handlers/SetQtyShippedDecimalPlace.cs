﻿namespace Extensions.Services.OrderHistoryLine.Handlers
{
    using System;
    using Insite.Catalog.Services;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services.Handlers;
    using Insite.Order.Services.Parameters;
    using Insite.Order.Services.Results;

    [DependencyName(nameof(SetQtyShippedDecimalPlace))]
    public sealed class SetQtyShippedDecimalPlace : HandlerBase<GetOrderParameter, GetOrderResult>
    {
        private readonly Lazy<IProductService> productService;

        public SetQtyShippedDecimalPlace(Lazy<IProductService> productService)
        {
            this.productService = productService;
        }

        public override int Order => 1401;

        public override GetOrderResult Execute(IUnitOfWork unitOfWork, GetOrderParameter parameter, GetOrderResult result)
        {
            if (!parameter.GetOrderLines)
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }          

            foreach (var orderLine in result.OrderHistory.OrderHistoryLines)
            {
                
                orderLine.QtyShipped = decimal.Truncate(decimal.Round(orderLine.QtyShipped, 0));
            }

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}