﻿using System;
using System.Collections.Generic;
using System.Linq;

using Insite.Cart.Services.Pipelines.Parameters;
using Insite.Cart.Services.Pipelines.Results;
using Insite.Core.ApplicationDictionary;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Pipelines;
using Insite.Data.Entities;

namespace Extensions.Services.Order.Pipes
{
    public class PopulatePlacedByCustomProperty : IPipe<CreateOrderHistoryParameter, CreateOrderHistoryResult>
    {
        private readonly IEntityDefinitionProvider entityDefinitionProvider;

        public int Order => 301;

        public PopulatePlacedByCustomProperty(IEntityDefinitionProvider entityDefinitionProvider)
        {
            this.entityDefinitionProvider = entityDefinitionProvider;
        }

        public CreateOrderHistoryResult Execute(IUnitOfWork unitOfWork, CreateOrderHistoryParameter parameter, CreateOrderHistoryResult result)
        {
            var customPropertiesToCopy = this.GetCustomPropertiesToCopy();

            foreach (var customProperty in parameter.CustomerOrder.CustomProperties.Where(o => customPropertiesToCopy.Contains(o.Name)))
            {
                result.OrderHistory.SetProperty(customProperty.Name, customProperty.Value);
            }

            return result;
        }

        private List<string> GetCustomPropertiesToCopy()
        {
            var customerOrderPropertyDefinitions = this.entityDefinitionProvider.GetByName(nameof(CustomerOrder), null)
                .Properties.Where(o => o.IsCustomProperty).ToList();
            var orderHistoryPropertyDefinitions = this.entityDefinitionProvider.GetByName(nameof(OrderHistory), null)
                .Properties.Where(o => o.IsCustomProperty).ToList();

            return customerOrderPropertyDefinitions
                .Where(
                    o => orderHistoryPropertyDefinitions.Any(
                        p => p.Name.Equals(o.Name, StringComparison.OrdinalIgnoreCase)
                             && p.PropertyType == o.PropertyType))
                .Select(o => o.Name)
                .ToList();
        }
    }
}