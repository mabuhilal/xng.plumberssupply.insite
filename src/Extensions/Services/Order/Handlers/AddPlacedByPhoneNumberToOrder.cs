﻿namespace Extensions.Services.Order.Handlers
{
    using System.Linq;

    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services.Handlers;
    using Insite.Data.Entities;
    using Insite.Order.Services.Parameters;
    using Insite.Order.Services.Results;

    [DependencyName(nameof(AddPlacedByPhoneNumberToOrder))]
    public sealed class AddPlacedByPhoneNumberToOrder : HandlerBase<GetOrderParameter, GetOrderResult>
    {
        public override int Order => 1106;

        public override GetOrderResult Execute(IUnitOfWork unitOfWork, GetOrderParameter parameter, GetOrderResult result)
        {
            if (string.IsNullOrEmpty(parameter.OrderNumber))
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            var customerOrder = unitOfWork.GetRepository<CustomerOrder>().GetTable()
                .FirstOrDefault(o => o.OrderNumber == parameter.OrderNumber || o.ErpOrderNumber == parameter.OrderNumber);

            if (customerOrder == null || customerOrder.PlacedByUserProfile == null || string.IsNullOrEmpty(customerOrder.PlacedByUserProfile.FirstName))
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            var plumbersPlacedByPhoneVal = customerOrder.GetProperty(PlumbersConstants.PlacedByPhoneNumberKey, string.Empty);

            if (!string.IsNullOrEmpty(plumbersPlacedByPhoneVal))
            {
                customerOrder.PlacedByUserName = plumbersPlacedByPhoneVal;
                unitOfWork.Save();
            }

            result.Properties.Add(
                PlumbersConstants.PlacedByPhoneNumberKey,
                $"{customerOrder.GetProperty(PlumbersConstants.PlacedByPhoneNumberKey, string.Empty)}"
            );

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}