﻿namespace Extensions.Services.Order.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Insite.Catalog.Services;
    using Insite.Catalog.Services.Dtos;
    using Insite.Catalog.Services.Parameters;
    using Insite.Common.Helpers;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services.Handlers;
    using Insite.Data.Entities;
    using Insite.Order.Services.Dtos;
    using Insite.Order.Services.Parameters;
    using Insite.Order.Services.Results;

    [DependencyName(nameof(GetLines))]
    public sealed class GetLines : HandlerBase<GetOrderParameter, GetOrderResult>
    {
        private readonly Lazy<IProductService> productService;

        public GetLines(Lazy<IProductService> productService)
        {
            this.productService = productService;
        }

        public override int Order => 1000;

        public override GetOrderResult Execute(IUnitOfWork unitOfWork, GetOrderParameter parameter, GetOrderResult result)
        {
            if (!parameter.GetOrderLines)
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            var productDtoDictionary = this.GetProductDtos(result.OrderHistory.OrderHistoryLines.ToList());
            foreach (var entry in productDtoDictionary)
            {
                var orderLineResult = new GetOrderLineResult { OrderHistoryLine = entry.Key, ProductDto = entry.Value };

                this.PopulateSectionOptions(entry, orderLineResult);

                result.GetOrderLineResults.Add(orderLineResult);
            }

            result.CanAddToCart = result.GetOrderLineResults.Any(o => o.ProductDto != null && o.ProductDto.CanAddToCart);
            result.CanAddAllToCart = !result.GetOrderLineResults.Any(o => o.ProductDto != null && !o.ProductDto.CanAddToCart);
            result.ShowTaxAndShipping = true;

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }

        private Dictionary<OrderHistoryLine, ProductDto> GetProductDtos(IList<OrderHistoryLine> orderHistoryLines)
        {
            var results = new Dictionary<OrderHistoryLine, ProductDto>();

            var getProductCollectionResult = this.productService.Value.GetProductCollection(new GetProductCollectionParameter
            {
                ErpNumbers = orderHistoryLines.Where(o => !o.ProductErpNumber.IsBlank()).Select(o => o.ProductErpNumber).ToList(),
                GetPrices = false,
                GetBrand = true
            });

            foreach (var orderHistoryLine in orderHistoryLines)
            {
                if (orderHistoryLine.ProductErpNumber.IsBlank() || getProductCollectionResult.ProductDtos == null)
                {
                    results.Add(orderHistoryLine, null);
                    continue;
                }

                var productDto = getProductCollectionResult.ProductDtos.FirstOrDefault(o => o.ERPNumber.SafeTrim().EqualsIgnoreCase(orderHistoryLine.ProductErpNumber.SafeTrim()));
                orderHistoryLine.UnitListPrice = NumberHelper.RoundCurrency(productDto?.BasicListPrice ?? 0);
                
                results.Add(orderHistoryLine, productDto);
            }

            return results;
        }

        private void PopulateSectionOptions(KeyValuePair<OrderHistoryLine, ProductDto> entry, GetOrderLineResult getOrderLineResult)
        {
            if (entry.Value == null || !entry.Value.IsConfigured)
            {
                return;
            }

            var configDataSet = XmlDatasetManager.ConvertXmlToDataset(entry.Key.ConfigDataSet);
            if (!configDataSet.Tables.Contains("SectionOption"))
            {
                return;
            }

            getOrderLineResult.SectionOptions = configDataSet.Tables["SectionOption"].Rows.Cast<DataRow>()
                .Where(row => entry.Value.IsFixedConfiguration || Convert.ToBoolean(row["Selected"] == DBNull.Value ? false : row["Selected"]))
                .OrderBy(row => Convert.ToInt32(row["SortOrder"] == DBNull.Value ? 0 : row["SortOrder"]))
                .Select(row =>
                    new SectionOptionDto(
                        new Guid(row["SectionOptionId"].ToString()),
                        row["SectionName"].ToString(),
                        row["Description"].ToString()))
                .ToList();
        }
    }
}