﻿namespace Extensions.Services.RealtimePricing.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Insite.Common.Logging;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Interfaces.Localization;
    using Insite.Core.Interfaces.Plugins.Caching;
    using Insite.Core.Interfaces.Plugins.Pricing;
    using Insite.Core.Plugins.Inventory;
    using Insite.Core.Plugins.Pricing;
    using Insite.Core.Providers;
    using Insite.Core.Services;
    using Insite.Core.Services.Handlers;
    using Insite.RealTimePricing.Engines;
    using Insite.RealTimePricing.SystemSettings;

    [DependencyName(nameof(SetRealtimeZeroPricing))]
    public sealed class SetRealtimeZeroPricing : HandlerBase<GetRealTimePricingParameter, GetRealTimePricingResult>
    {
        private readonly ICacheManager cacheManager;

        private readonly ITranslationLocalizer translationLocalizer;

        private readonly IRealTimePricingEngine realTimePricingEngine;

        private readonly RealTimePricingSettings realTimePricingSettings;

        public SetRealtimeZeroPricing(ICacheManager cacheManager, ITranslationLocalizer translationLocalizer, IRealTimePricingEngine realTimePricingEngine, RealTimePricingSettings realTimePricingSettings)
        {
            this.cacheManager = cacheManager;
            this.translationLocalizer = translationLocalizer;
            this.realTimePricingEngine = realTimePricingEngine;
            this.realTimePricingSettings = realTimePricingSettings;
        }

        public override int Order => 701;

        public override GetRealTimePricingResult Execute(IUnitOfWork unitOfWork, GetRealTimePricingParameter parameter, GetRealTimePricingResult result)
        {
            foreach (var kvp in result.RealTimePricingResults)
            {
                if (kvp.Value.UnitRegularPrice <= 0)
                {
                    kvp.Value.UnitNetPriceDisplay = this.translationLocalizer.TranslateLabel(PlumbersConstants.PlumbersProductIsNotAvailableKey);
                    kvp.Value.UnitOfMeasure = string.Empty;
                }
            }

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}