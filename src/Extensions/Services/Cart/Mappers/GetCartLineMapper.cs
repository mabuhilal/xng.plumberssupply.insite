﻿namespace Extensions.Services.Cart.Mappers
{
    using System;
    using System.Net.Http;

    using Insite.Cart.Services.Parameters;
    using Insite.Cart.Services.Results;
    using Insite.Cart.WebApi.V1.ApiModels;
    using Insite.Cart.WebApi.V1.Mappers;
    using Insite.Cart.WebApi.V1.Mappers.Interfaces;
    using Insite.Catalog.Services.Dtos;
    using Insite.Core.Extensions;
    using Insite.Core.Interfaces.Localization;
    using Insite.Core.Plugins.Pricing;
    using Insite.Core.Plugins.Utilities;
    using Insite.Core.WebApi.Interfaces;

    /// <summary>Maps the parameter for calling the service and the result returned from the service.</summary>
    public class PlumbersGetCartLineMapper : GetCartLineMapper
    {
        private readonly ITranslationLocalizer translationLocalizer;

        public PlumbersGetCartLineMapper(
            ICurrencyFormatProvider currencyFormatProvider,
            IObjectToObjectMapper objectToObjectMapper,
            IUrlHelper urlHelper,
            IRouteDataProvider routeDataProvider,
            ITranslationLocalizer translationLocalizer) : base(currencyFormatProvider, objectToObjectMapper, urlHelper, routeDataProvider)
        {
            this.translationLocalizer = translationLocalizer;
        }

        public override CartLineModel MapResult(GetCartLineResult serviceResult, HttpRequestMessage request)
        {
            var cartLineModel = base.MapResult(serviceResult, request);

            if (cartLineModel.Pricing.UnitNetPrice <= 0)
            {
                this.translationLocalizer.TranslateLabel(PlumbersConstants.PlumbersProductIsNotAvailableKey);
            }

            return cartLineModel;
        }
    }
}