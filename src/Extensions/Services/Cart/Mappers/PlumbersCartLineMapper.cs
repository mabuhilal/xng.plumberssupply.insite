﻿using Extensions.SystemSettings.Group;
using Insite.Cart.Services.Results;
using Insite.Cart.WebApi.V1.ApiModels;
using Insite.Cart.WebApi.V1.Mappers;
using Insite.Cart.WebApi.V1.Mappers.Interfaces;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Utilities;
using Insite.Core.Providers;
using Insite.Core.WebApi.Interfaces;
using Insite.Customers.WebApi.V1.Mappers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Extensions.Services.Cart.Mappers
{
    public class PlumbersGetCartMapper : GetCartMapper
    {
        private PlumbersCheckoutSettings PlumbersCheckoutSettings;
        public PlumbersGetCartMapper(
            ICurrencyFormatProvider currencyFormatProvider,
            IGetBillToMapper getBillToMapper,
            IGetShipToMapper getShipToMapper,
            IGetCartLineCollectionMapper getCartLineCollectionMapper,
            IObjectToObjectMapper objectToObjectMapper,
            IUrlHelper urlHelper,
            IRouteDataProvider routeDataProvider,
            ITranslationLocalizer translationLocalizer,
            PlumbersCheckoutSettings plumbersCheckoutSettings) : base(currencyFormatProvider, getBillToMapper, getShipToMapper, getCartLineCollectionMapper, objectToObjectMapper, urlHelper, routeDataProvider, translationLocalizer)
        {
            this.PlumbersCheckoutSettings = plumbersCheckoutSettings;
        }

        public override CartModel MapResult(GetCartResult serviceResult, HttpRequestMessage request)
        {
            var result = base.MapResult(serviceResult, request);
            if (result.PaymentOptions?.PaymentMethods != null)
            {
                foreach (var paymentMethod in result.PaymentOptions.PaymentMethods)
                {
                    if (!paymentMethod.Description.EqualsIgnoreCase(this.PlumbersCheckoutSettings.VisiblePaymentMethodName))
                    {
                        paymentMethod.Description = MessageProvider.Current.GetMessage(PlumbersConstants.PaymentMethodMaskText, PlumbersConstants.PlumbersOnAccountKey);
                    }
                }
            }
            return result;
        }
    }

}