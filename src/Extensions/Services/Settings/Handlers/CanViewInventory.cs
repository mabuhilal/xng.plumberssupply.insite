﻿namespace Extensions.Services.Settings.Handlers
{
    using Extensions.Models;
    using Insite.Catalog.Services.Results;
    using Insite.Core.Context;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Services;
    using Insite.Core.Services.Handlers;

    [DependencyName(nameof(CanViewInventory))]
    public sealed class CanViewInventory : HandlerBase<GetSettingsParameter, GetProductSettingsResult>
    {
        public override int Order => 510;

        public override GetProductSettingsResult Execute(IUnitOfWork unitOfWork, GetSettingsParameter parameter, GetProductSettingsResult result)
        {
            if (SiteContext.Current.BillTo == null)
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            var canCustomerViewInventory = SiteContext.Current.BillTo.GetProperty(PlumbersConstants.CustomerCanViewInventoryKey, string.Empty);
            var customerSegment = SiteContext.Current.BillTo.GetProperty(PlumbersConstants.PlumbersCustomerSegmentKey, string.Empty);

            // CanViewInventory customer setting takes precedence over the wholesale setting.
            if (!string.IsNullOrEmpty(customerSegment) && customerSegment.ToLower() != "wholesale")
            {
                result.ShowInventoryAvailability = true;
            }

            if (canCustomerViewInventory == null || canCustomerViewInventory == "true")
            {
                result.ShowInventoryAvailability = true;
            } 
            else
            {
                result.ShowInventoryAvailability = false;
            }

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}