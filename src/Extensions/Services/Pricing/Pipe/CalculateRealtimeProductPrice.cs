﻿namespace Extensions.Services.Pricing.Pipe
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Localization;
    using Insite.Core.Plugins.EntityUtilities;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Core.Plugins.Pipelines.Pricing.Parameters;
    using Insite.Core.Plugins.Pipelines.Pricing.Results;
    using Insite.Core.Plugins.Pricing;
    using Insite.Core.Services;
    using Insite.Core.SystemSetting.Groups.Catalog;

    /// <summary>Calculates real time pricing for a collection of PricingServiceParameters.</summary>
    public sealed class CalculateProductRealTimePricing : IPipe<GetProductPricingParameter, GetProductPricingResult>
    {
        private readonly PricingSettings pricingSettings;
        private readonly Lazy<IRealTimePricingService> realTimePricingService;
        private readonly IOrderLineUtilities orderLineUtilities;
        private readonly ITranslationLocalizer translationLocalizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculateProductRealTimePricing"/> class.
        /// Calculate real time pricing for a cart.
        /// </summary>
        /// <param name="pricingSettings">The price settings.</param>
        /// <param name="realTimePricingService">The real time pricing service.</param>
        /// <param name="orderLineUtilities">The order line utilities.</param>
        public CalculateProductRealTimePricing(Lazy<IRealTimePricingService> realTimePricingService, PricingSettings pricingSettings, IOrderLineUtilities orderLineUtilities, ITranslationLocalizer translationLocalizer)
        {
            this.realTimePricingService = realTimePricingService;
            this.pricingSettings = pricingSettings;
            this.orderLineUtilities = orderLineUtilities;
            this.translationLocalizer = translationLocalizer;
        }

        /// <summary>
        /// The order this pipe executes in the pipeline.
        /// </summary>
        public int Order => 150;

        /// <summary>
        /// Calculates real time pricing for a collection of products.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="parameter">The <see cref="GetProductPricingParameter"/> parameter.</param>
        /// <param name="result">The <see cref="GetProductPricingResult"/> result.</param>
        /// <returns>The <see cref="GetProductPricingResult"/>.</returns>
        public GetProductPricingResult Execute(IUnitOfWork unitOfWork, GetProductPricingParameter parameter, GetProductPricingResult result)
        {
            var getRealTimePricingParameter = this.GetRealTimePricingParameter(parameter);
            var anyProductPriceDtoToSkip = result.ProductPriceDtos?.Any(o => !o.Value.RequiresRealTimePrice) ?? false;
            if (anyProductPriceDtoToSkip)
            {
                foreach (var productPriceDto in result.ProductPriceDtos.Where(o => !o.Value.RequiresRealTimePrice))
                {
                    if (getRealTimePricingParameter.ProductPriceParameters.ContainsKey(productPriceDto.Key))
                    {
                        getRealTimePricingParameter.ProductPriceParameters.Remove(productPriceDto.Key);
                    }
                }

                if (!getRealTimePricingParameter.ProductPriceParameters.Any())
                {
                    return result;
                }
            }

            var serviceResult = this.realTimePricingService.Value.GetPricing(getRealTimePricingParameter);
            if (serviceResult.ResultCode == ResultCode.Error)
            {
                result.ResultCode = serviceResult.ResultCode;
                result.SubCode = serviceResult.SubCode;
                result.Messages = serviceResult.Messages;
                return result;
            }

            if (anyProductPriceDtoToSkip)
            {
                foreach (var realTimePricingResult in serviceResult.RealTimePricingResults)
                {
                    if (result.ProductPriceDtos.ContainsKey(realTimePricingResult.Key))
                    {
                        result.ProductPriceDtos[realTimePricingResult.Key] = realTimePricingResult.Value;
                    }
                    else
                    {
                        result.ProductPriceDtos.Add(realTimePricingResult);
                    }
                }
            }
            else
            {
                result.ProductPriceDtos = serviceResult.RealTimePricingResults;
            }

            foreach (var kvp in result.ProductPriceDtos)
            {
                if (kvp.Value.UnitRegularPrice <= 0)
                {
                    kvp.Value.UnitNetPriceDisplay = this.translationLocalizer.TranslateLabel(PlumbersConstants.PlumbersProductIsNotAvailableKey);
                    kvp.Value.UnitOfMeasure = string.Empty;
                }
            }

            return result;
        }

        /// <summary>
        /// Get real time pricing parameter.
        /// </summary>
        /// <param name="parameter">The <see cref="GetProductPricingParameter"/> parameter.</param>
        /// <returns>The <see cref="GetRealTimePricingParameter"/>.</returns>
        private GetRealTimePricingParameter GetRealTimePricingParameter(GetProductPricingParameter parameter)
        {
            var getRealTimePricingParameter = new GetRealTimePricingParameter { ProductPriceParameters = new Dictionary<Guid, ProductPriceParameter>() };
            foreach (var priceParameter in parameter.PricingServiceParameters)
            {
                var productPriceParameter = new ProductPriceParameter
                {
                    ProductId = priceParameter.Value.ProductId.ToString(),
                    QtyOrdered = priceParameter.Value.QtyOrdered,
                    UnitOfMeasure = priceParameter.Value.UnitOfMeasure
                };

                if (priceParameter.Value.OrderLine != null)
                {
                    productPriceParameter.QtyOrdered = priceParameter.Value.OrderLine.QtyOrdered;
                    productPriceParameter.UnitOfMeasure = priceParameter.Value.OrderLine.UnitOfMeasure;
                    productPriceParameter.Configuration = this.orderLineUtilities.GetSectionOptions(priceParameter.Value.OrderLine).ToList();
                }

                getRealTimePricingParameter.ProductPriceParameters.Add(priceParameter.Key, productPriceParameter);
            }

            return getRealTimePricingParameter;
        }
    }
}