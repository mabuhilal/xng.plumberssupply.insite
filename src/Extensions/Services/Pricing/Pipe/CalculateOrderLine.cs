﻿namespace Extensions.Services.Pricing.Pipe
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Insite.Common.Helpers;
    using Insite.Common.Providers;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Plugins.Pricing;
    using Insite.Core.Plugins.EntityUtilities;
    using Insite.Core.Plugins.Pipelines;
    using Insite.Core.Plugins.Pipelines.Pricing;
    using Insite.Core.Plugins.Pipelines.Pricing.Parameters;
    using Insite.Core.Plugins.Pipelines.Pricing.Results;
    using Insite.Core.Services;
    using Insite.Core.SystemSetting.Groups.OrderManagement;
    using Insite.Data.Entities;

    /// <summary>This pipe calculates order line prices.</summary>
    public sealed class CalculateOrderLines : IPipe<GetCartPricingParameter, GetCartPricingResult>
    {
        /// <summary> The product utilities </summary>
        private readonly Lazy<IProductUtilities> productUtilities;

        private readonly BudgetsAndOrderApprovalSettings budgetsAndOrderApprovalSettings;

        /// <summary> The pricing pipeline </summary>
        private readonly IPricingPipeline pricingPipeline;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculateOrderLines"/> class.
        /// </summary>
        /// <param name="productUtilities">The product utilities.</param>
        /// <param name="budgetsAndOrderApprovalSettings">The budgets and order approval settings.</param>
        /// <param name="pricingPipeline">The pricing pipeline.</param>
        public CalculateOrderLines(Lazy<IProductUtilities> productUtilities, BudgetsAndOrderApprovalSettings budgetsAndOrderApprovalSettings, IPricingPipeline pricingPipeline)
        {
            this.productUtilities = productUtilities;
            this.budgetsAndOrderApprovalSettings = budgetsAndOrderApprovalSettings;
            this.pricingPipeline = pricingPipeline;
        }

        /// <summary>
        /// The order this pipe executes in the pipeline.
        /// </summary>
        public int Order => 100;

        /// <summary>
        /// Calculates the order lines for the cart.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="parameter">The <see cref="GetCartPricingParameter"/> parameter.</param>
        /// <param name="result">The <see cref="GetCartPricingResult"/> result.</param>
        /// <returns>The <see cref="GetCartPricingResult"/>.</returns>
        public GetCartPricingResult Execute(IUnitOfWork unitOfWork, GetCartPricingParameter parameter, GetCartPricingResult result)
        {
            result.OrderLinesToPrice = parameter.Cart.OrderLines.Where(o => this.ShouldOrderLineBePriced(o, parameter.ForceRecalculation)
                && (parameter.OrderLineId == null || o.Id == parameter.OrderLineId.Value)).ToList();
            if (!result.OrderLinesToPrice.Any())
            {
                return result;
            }

            var getProductPricingResult = this.CalculatePrice(result.OrderLinesToPrice);
            if (getProductPricingResult.ResultCode != ResultCode.Success)
            {
                return PipelineHelper.CreateErrorPipelineResult(result, getProductPricingResult.SubCode, getProductPricingResult.Message);
            }

            parameter.Cart.RecalculatePromotions = true;
            parameter.Cart.ShippingCalculationNeededAsOf = parameter.CalculateShipping ? DateTimeProvider.Current.Now : parameter.Cart.ShippingCalculationNeededAsOf;
            parameter.Cart.RecalculateTax = parameter.CalculateTaxes || parameter.Cart.RecalculateTax;

            return result;
        }

        /// <summary>Calculates the price. </summary>
        /// <param name="orderLines">The order lines to price.</param>
        /// <returns>The result from getting the product pricing for the order lines.</returns>
        private GetProductPricingResult CalculatePrice(IList<OrderLine> orderLines)
        {
            var getProductPricingParameter = new GetProductPricingParameter(false)
            {
                PricingServiceParameters = new Dictionary<Guid, PricingServiceParameter>()
            };

            foreach (var orderLine in orderLines)
            {
                getProductPricingParameter.PricingServiceParameters.Add(orderLine.Id, new PricingServiceParameter(orderLine.Product.Id) { CustomerOrderId = orderLine.CustomerOrderId, OrderLine = orderLine });
            }

            var result = this.pricingPipeline.GetProductPricing(getProductPricingParameter);

            if (result.ResultCode != ResultCode.Success)
            {
                return result;
            }

            foreach (var pricingResult in result.ProductPriceDtos)
            {
                var orderLine = orderLines.First(o => o.Id.Equals(pricingResult.Key));
                var productPriceDto = pricingResult.Value;
                orderLine.UnitListPrice = productPriceDto.UnitListPrice;
                orderLine.UnitRegularPrice = productPriceDto.UnitRegularPrice;
                orderLine.UnitNetPrice = orderLine.UnitRegularPrice;
                orderLine.TotalRegularPrice = this.GetTotalRegularPrice(orderLine);
                orderLine.TotalNetPrice = this.GetTotalNetPrice(orderLine);
            }

            return result;
        }

        /// <summary>Orders the line should be priced.</summary>
        /// <param name="orderLine">The order line.</param>
        /// <param name="forceRecalculate">if set to <c>true</c> [force recalculate].</param>
        /// <returns>True if the order line should be priced.</returns>
        private bool ShouldOrderLineBePriced(OrderLine orderLine, bool forceRecalculate)
        {
            if (forceRecalculate)
            {
                return true;
            }

            if (this.IsInAwaitingApprovalStatusAndNotUpdatePricingOnApproval(orderLine))
            {
                return false;
            }

            if (orderLine.IsPromotionItem)
            {
                return false;
            }

            if (this.IsConfiguredAndNotInQuoteProposedStatus(orderLine))
            {
                return false;
            }

            if (this.IsQuoteRequiredAndNotInQuoteProposedAndNotInAwaitingApprovalStatus(orderLine))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the order line is in awaiting approval status and we should not update pricing on approval.
        /// </summary>
        /// <param name="orderLine">The order line.</param>
        /// <returns>True if the order line is in awaiting approval status and we should not update pricing on approval.</returns>
        private bool IsInAwaitingApprovalStatusAndNotUpdatePricingOnApproval(OrderLine orderLine)
        {
            return orderLine.CreatedOn != default(DateTimeOffset) &&
                orderLine.CustomerOrder.Status.EqualsIgnoreCase(CustomerOrder.StatusType.AwaitingApproval) &&
                !this.budgetsAndOrderApprovalSettings.UpdatePricingOnApproval;
        }

        /// <summary>Determines whether the order line is configured and not in quote proposed status.</summary>
        /// <param name="orderLine">The order line.</param>
        /// <returns>True if the order line is configured and not in quote proposed status.</returns>
        private bool IsConfiguredAndNotInQuoteProposedStatus(OrderLine orderLine)
        {
            return orderLine.Product.ConfigurationObject != null && !orderLine.CustomerOrder.Status.EqualsIgnoreCase(CustomerOrder.StatusType.QuoteProposed);
        }

        /// <summary>
        /// Determines whether the orderline is quote required and not in quote proposed and not in awaiting approval status.
        /// </summary>
        /// <param name="orderLine">The order line.</param>
        /// <returns>True if the orderline is quote required and not in quote proposed and not in awaiting approval status.</returns>
        private bool IsQuoteRequiredAndNotInQuoteProposedAndNotInAwaitingApprovalStatus(OrderLine orderLine)
        {
            return this.productUtilities.Value.IsQuoteRequired(orderLine.Product) &&
                !orderLine.CustomerOrder.Status.EqualsIgnoreCase(CustomerOrder.StatusType.QuoteProposed) &&
                !orderLine.CustomerOrder.Status.EqualsIgnoreCase(CustomerOrder.StatusType.AwaitingApproval);
        }

        /// <summary>Returns the <see cref="OrderLine"/>.TotalRegularPrice.</summary>
        /// <param name="orderLine">The order line.</param>
        /// <returns>The <see cref="decimal"/>.</returns>
        private decimal GetTotalRegularPrice(OrderLine orderLine)
        {
            return NumberHelper.RoundCurrency(orderLine.UnitRegularPrice * orderLine.QtyOrdered);
        }

        /// <summary>Returns the <see cref="OrderLine"/>.TotalNetPrice.</summary>
        /// <param name="orderLine">The order line.</param>
        /// <returns>The <see cref="decimal"/>.</returns>
        private decimal GetTotalNetPrice(OrderLine orderLine)
        {
            return NumberHelper.RoundCurrency(orderLine.UnitNetPrice * orderLine.QtyOrdered);
        }
    }
}