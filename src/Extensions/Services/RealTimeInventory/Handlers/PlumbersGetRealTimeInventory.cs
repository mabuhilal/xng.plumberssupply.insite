﻿namespace Extensions.Services.RealTimeInventory.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Insite.Catalog.Services.Dtos;
    using Insite.Catalog.Services.Parameters;
    using Insite.Catalog.Services.Pipelines;
    using Insite.Catalog.Services.Pipelines.Parameters;
    using Insite.Core.Context;
    using Insite.Core.Interfaces.Data;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Interfaces.Localization;
    using Insite.Core.Plugins.Inventory;
    using Insite.Core.Services;
    using Insite.Core.Services.Handlers;
    using Insite.Data.Entities;
    using Insite.Data.Extensions;
    using Insite.Data.Repositories.Interfaces;
    using Insite.RealTimeInventory.Services.Parameters;
    using Insite.RealTimeInventory.Services.Results;

    [DependencyName(nameof(GetAvailability))]
    public sealed class GetAvailability : HandlerBase<GetRealTimeInventoryParameter, GetRealTimeInventoryResult>
    {
        private readonly ICatalogPipeline catalogPipeline;
        private readonly ITranslationLocalizer translationLocalizer;

        public GetAvailability(ICatalogPipeline catalogPipeline, ITranslationLocalizer translationLocalizer)
        {
            this.catalogPipeline = catalogPipeline;
            this.translationLocalizer = translationLocalizer;
        }

        public override int Order => 800;

        public override GetRealTimeInventoryResult Execute(IUnitOfWork unitOfWork, GetRealTimeInventoryParameter parameter, GetRealTimeInventoryResult result)
        {
            var productDictionary = GetProductDictionary(unitOfWork, result);
            var warehouses = unitOfWork.GetTypedRepository<IWarehouseRepository>().GetCachedWarehouses().ToList();

            foreach (var realTimeInventoryResult in result.RealTimeInventoryResults)
            {
                var product = productDictionary[realTimeInventoryResult.Key];
                var productInventory = realTimeInventoryResult.Value;
                var configuration = parameter.GetInventoryParameter.Configurations.ContainsKey(product.Id)
                    ? parameter.GetInventoryParameter.Configurations[product.Id]
                    : new List<Guid>();

                this.ProcessAvailability(result, productInventory, product, product.UnitOfMeasure, 1, configuration);
                this.ProcessAvailabilityByWarehouse(result, productInventory, product, product.UnitOfMeasure, 1, configuration, warehouses);

                if (product.UnitOfMeasure.IsBlank())
                {
                    continue;
                }

                foreach (var productUnitOfMeasure in product.ProductUnitOfMeasures.Where(o => o.QtyPerBaseUnitOfMeasure > 0))
                {
                    this.ProcessAvailability(result, productInventory, product, productUnitOfMeasure.UnitOfMeasure, productUnitOfMeasure.QtyPerBaseUnitOfMeasure, configuration);
                    this.ProcessAvailabilityByWarehouse(result, productInventory, product, productUnitOfMeasure.UnitOfMeasure, productUnitOfMeasure.QtyPerBaseUnitOfMeasure, configuration, warehouses);
                }
            }

            return this.NextHandler.Execute(unitOfWork, parameter, result);
        }

        private static Dictionary<Guid, Product> GetProductDictionary(IUnitOfWork unitOfWork, GetRealTimeInventoryResult result)
        {
            var productIds = result.RealTimeInventoryResults.Select(r => r.Key);

            return unitOfWork.GetRepository<Product>().GetTableAsNoTracking()
                .Expand(o => o.ProductUnitOfMeasures)
                .Expand(o => o.ProductWarehouses.Select(p => p.Warehouse))
                .WhereContains(o => o.Id, productIds)
                .ToDictionary(o => o.Id, o => o);
        }

        private void ProcessAvailability(GetRealTimeInventoryResult result, ProductInventory productInventory, Product product, string unitOfMeasure, decimal qtyPerBaseUnitOfMeasure, List<Guid> configuration)
        {
            if (productInventory.InventoryAvailabilityDtos.Any(o => o.UnitOfMeasure.Equals(unitOfMeasure)))
            {
                return;
            }

            var availabilityDto = this.GetAvailabilityForUnitOfMeasure(result, productInventory, product, qtyPerBaseUnitOfMeasure, productInventory.QtyOnHand, configuration);
            if (availabilityDto == null)
            {
                return;
            }

            var customerSegment = SiteContext.Current.BillTo.GetProperty(PlumbersConstants.PlumbersCustomerSegmentKey, string.Empty);

            if (customerSegment.ToLower() == "wholesale" && availabilityDto.MessageType != AvailabilityMessageType.OutOfStock)
            {
                availabilityDto.Message = SiteContext.Current.FulfillmentMethod == "PickUp" 
                    ? this.translationLocalizer.TranslateLabel($"In Stock at {SiteContext.Current.PickUpWarehouseDto?.Description}")
                    : this.translationLocalizer.TranslateLabel($"In Stock");
            }
            else if (customerSegment.ToLower() == "wholesale" && availabilityDto.MessageType == AvailabilityMessageType.OutOfStock)
            {
                availabilityDto.Message = SiteContext.Current.FulfillmentMethod == "PickUp"
                    ? this.translationLocalizer.TranslateLabel($"Out of Stock at {SiteContext.Current.PickUpWarehouseDto?.Description}")
                    : this.translationLocalizer.TranslateLabel($"Out of Stock");
            }
            else if (customerSegment.ToLower() != "wholesale" && availabilityDto.MessageType == AvailabilityMessageType.OutOfStock)
            {
                availabilityDto.Message = SiteContext.Current.FulfillmentMethod == "PickUp"
                    ? this.translationLocalizer.TranslateLabel($"Out of Stock at {SiteContext.Current.PickUpWarehouseDto?.Description}")
                    : this.translationLocalizer.TranslateLabel($"Out of Stock");
            }

            productInventory.InventoryAvailabilityDtos.Add(new InventoryAvailabilityDto { UnitOfMeasure = unitOfMeasure, Availability = availabilityDto });
        }

        private void ProcessAvailabilityByWarehouse(GetRealTimeInventoryResult result, ProductInventory productInventory, Product product, string unitOfMeasure, decimal qtyPerBaseUnitOfMeasure, List<Guid> configurations, List<Insite.Data.Entities.Dtos.WarehouseDto> warehouses)
        {
            if (!productInventory.WarehouseQtyOnHandDtos.Any())
            {
                return;
            }

            var inventoryWarehousesDto = productInventory.InventoryWarehousesDtos.FirstOrDefault(o => o.UnitOfMeasure.EqualsIgnoreCase(unitOfMeasure));
            if (inventoryWarehousesDto == null)
            {
                inventoryWarehousesDto = new InventoryWarehousesDto { UnitOfMeasure = unitOfMeasure };
                productInventory.InventoryWarehousesDtos.Add(inventoryWarehousesDto);
            }

            foreach (var warehouseQtyOnHandDto in productInventory.WarehouseQtyOnHandDtos)
            {
                if (productInventory.InventoryWarehousesDtos.Any(o => o.UnitOfMeasure.EqualsIgnoreCase(unitOfMeasure)
                    && o.WarehouseDtos.Any(p => p.Name.EqualsIgnoreCase(warehouseQtyOnHandDto.Name))))
                {
                    continue;
                }

                var availabilityDto = this.GetAvailabilityForUnitOfMeasure(result, productInventory, product, qtyPerBaseUnitOfMeasure, warehouseQtyOnHandDto.QtyOnHand, configurations);
                if (availabilityDto == null)
                {
                    continue;
                }

                var warehouse = warehouses.FirstOrDefault(o => o.Name.Equals(warehouseQtyOnHandDto.Name, StringComparison.OrdinalIgnoreCase));
                if (warehouse == null)
                {
                    continue;
                }

                var warehouseDto = new WarehouseDto(availabilityDto)
                {
                    Name = warehouseQtyOnHandDto.Name,
                    Description = warehouse.Description,
                    Qty = warehouseQtyOnHandDto.QtyOnHand / qtyPerBaseUnitOfMeasure
                };
                    
                var customerSegment = SiteContext.Current.BillTo.GetProperty(PlumbersConstants.PlumbersCustomerSegmentKey, string.Empty);
                warehouseDto.Properties[PlumbersConstants.PlumbersCustomerSegmentKey] = customerSegment;

                if (customerSegment.ToLower() == "wholesale" && warehouseDto.MessageType != AvailabilityMessageType.OutOfStock)
                {
                    warehouseDto.Message = this.translationLocalizer.TranslateLabel($"In Stock");
                }
                else if (customerSegment.ToLower() == "wholesale" && warehouseDto.MessageType == AvailabilityMessageType.OutOfStock)
                {
                    warehouseDto.Message = "Out of Stock";
                }
                else if (customerSegment.ToLower() != "wholesale" && warehouseDto.MessageType == AvailabilityMessageType.OutOfStock)
                {
                    warehouseDto.Message = this.translationLocalizer.TranslateLabel($"Out of Stock");
                }
                else if (customerSegment.ToLower() != "wholesale" && warehouseDto.MessageType != AvailabilityMessageType.OutOfStock)
                {
                    warehouseDto.Message = this.translationLocalizer.TranslateLabel($"{warehouseDto.Qty} In Stock");
                }

                inventoryWarehousesDto.WarehouseDtos.Add(warehouseDto);
            }
        }

        private AvailabilityDto GetAvailabilityForUnitOfMeasure(GetRealTimeInventoryResult result, ProductInventory productInventory, Product product, decimal qtyPerBaseUnitOfMeasure, decimal qtyOnHand, List<Guid> configuration)
        {
            if (productInventory.AvailabilityDto != null)
            {
                return productInventory.AvailabilityDto;
            }

            var availableQty = qtyOnHand / qtyPerBaseUnitOfMeasure;
            var populateAvailabilitiesResult = this.catalogPipeline.PopulateAvailabilities(new PopulateAvailabilitiesParameter(SiteContext.Current, GetProductAvailabilityParameterDtos(product, availableQty), result.GetProductSettingsResult));
            if (populateAvailabilitiesResult.ResultCode != ResultCode.Success)
            {
                this.CreateErrorServiceResult(result, populateAvailabilitiesResult.SubCode, populateAvailabilitiesResult.Message);
                return null;
            }

            populateAvailabilitiesResult.PopulateAvailabilitiesDtos.TryGetValue(product.Id, out PopulateAvailabilitiesDto populateAvailabilitiesDto);
            return populateAvailabilitiesDto.AvailabilityDto;
        }

        private static List<ProductAvailabilityParameterDto> GetProductAvailabilityParameterDtos(Product product, decimal qtyOnHand)
        {
            return new List<ProductAvailabilityParameterDto>
            {
                new ProductAvailabilityParameterDto
                {
                    Product = product,
                    AvailableQty = qtyOnHand
                }
            };
        }
    }
}