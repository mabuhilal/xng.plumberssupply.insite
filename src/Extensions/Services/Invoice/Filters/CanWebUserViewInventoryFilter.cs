﻿namespace Extensions.Services.Invoice.Filters
{
    using System.Collections.Generic;

    using Insite.ContentLibrary.Filters;
    using Insite.Core.Interfaces.Plugins.Security;
    using Insite.Core.Plugins.EntityUtilities;
    using Insite.Invoice.Content;
    using Insite.WebFramework.Mvc;
    
    using Extensions.Models.CustomRoles;
    using Insite.Core.Security;

    public class CanWebUserViewInventoryFilter : IsAuthorizedFilter<InvoiceContentPage>
    {
        public CanWebUserViewInventoryFilter(IActionResultFactory actionResultFactory, IAuthenticationService authenticationService, IUserProfileUtilities userProfileUtilities)
            : base(actionResultFactory, authenticationService, userProfileUtilities)
        {
        }

        public override IList<string> RolesAllowed => new[] { CustomRoles.Owner, BuiltInRoles.Administrator };

        public override IList<string> RolesDisallowed => new[] { CustomRoles.Employee };

        public override bool DisplayLinkForRememberedUser => true;
    }
}