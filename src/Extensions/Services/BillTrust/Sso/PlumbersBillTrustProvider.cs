﻿namespace Extensions.Services.BillTrust.Sso
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    using Insite.Common.Logging;
    using Insite.Core.Context;
    using Insite.Core.Interfaces.Dependency;
    using Insite.Core.Plugins.Sso;
    using Insite.Core.SystemSetting.Groups.SiteConfigurations;

    [DependencyName(nameof(PlumbersBillTrustProvider))]
    public sealed class PlumbersBillTrustProvider : IBillTrustProvider
    {
        private readonly BilltrustSsoSettings billtrustSsoSettings;

        public PlumbersBillTrustProvider(BilltrustSsoSettings billtrustSsoSettings)
        {
            this.billtrustSsoSettings = billtrustSsoSettings;
        }

        public string GetBillTrustInvoiceUrl()
        {
            var url = this.billtrustSsoSettings.TestMode ? this.billtrustSsoSettings.TestUrl : this.billtrustSsoSettings.ProductionUrl;

            var accountNumber = $"AN={SiteContext.Current.BillTo.CustomerNumber}";
            var emailAddress = $"EA={SiteContext.Current.UserProfileDto.Email}";
            var userName = $"UN={SiteContext.Current.UserProfileDto.UserName}_{SiteContext.Current.BillTo.CustomerNumber}";
            var paperBill = $"PB={(this.billtrustSsoSettings.EnablePaperBill ? "Y" : "N")}";
            var emailNotify = $"EN={(this.billtrustSsoSettings.EnableEmailNotify ? "Y" : "N")}";
            var emailConfirmations = $"EC={(this.billtrustSsoSettings.EnableEmailConfirmations ? "Y" : "N")}";
            var timestamp = $"TS={DateTime.Now:yyyy-MM-dd HH:mm:ss}";

            var btdata = $"{accountNumber};{emailAddress};{userName};{paperBill};{emailNotify};{emailConfirmations};{timestamp}";

            var encryptionKey = this.billtrustSsoSettings.EncryptionKey;
            url += $"?BTDATA='{(encryptionKey.IsBlank() ? btdata : this.Encrypt(btdata, encryptionKey))}'";
            url += $"&CG={this.billtrustSsoSettings.ClientGuid}&ETYPE={(encryptionKey.IsBlank() ? "0" : "1")}";

            return url;
        }

        private string Encrypt(string dataToEncrypt, string securityKey)
        {
            if (securityKey.Length < 8)
            {
                return string.Empty;
            }

            var data = Encoding.UTF8.GetBytes(dataToEncrypt);
            var key = Encoding.UTF8.GetBytes(securityKey);
            var initializationVector = Encoding.UTF8.GetBytes(securityKey.Substring(0, 8));

            var tripleDES = TripleDES.Create();
            tripleDES.IV = initializationVector;

            try
            {
                tripleDES.Key = key;
            }
            catch (CryptographicException ex)
            {
                LogHelper.For(this).Error($"Error on setting Key: {ex.Message}", ex);
                return string.Empty;
            }

            tripleDES.Padding = PaddingMode.Zeros;
            tripleDES.Mode = CipherMode.CBC;

            var cryptoTransform = tripleDES.CreateEncryptor();
            var result = cryptoTransform.TransformFinalBlock(data, 0, data.Length);

            return Convert.ToBase64String(result, 0, result.Length);
        }
    }
}