﻿using Insite.Core.Interfaces.Dependency;
using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Extensions.SystemSettings.Group
{
    [SettingsGroup(PrimaryGroupName = "Integration", Label  = "Plumbers Integration")]
    public class PlumbersIntegrationSettings : BaseSettingsGroup, IExtension
    {
        [SettingsField(DisplayName = "Default Order Approval Type", Description = "Defines the default order approval type value when submitting orders to SX.e .")]
        public virtual string DefaultOrderApprovalType => this.GetValue("Y");
    }
}