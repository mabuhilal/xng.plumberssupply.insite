﻿using Insite.Core.Interfaces.Dependency;
using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Extensions.SystemSettings.Group
{
    [SettingsGroup(PrimaryGroupName = "OrderManagement", Label  = "Plumbers Order Management")]
    public class PlumbersCheckoutSettings : BaseSettingsGroup, IExtension
    {
        [SettingsField(DisplayName = "Visible Payment Method Name", Description = "Defines which payment method description will be visible during checkout. All other payment methods will display the Payment_Method_Mask_Text site message.")]
        public virtual string VisiblePaymentMethodName => this.GetValue("COD");
    }
}