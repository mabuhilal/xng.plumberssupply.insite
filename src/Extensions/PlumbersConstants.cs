﻿namespace Extensions
{
    public class PlumbersConstants
    {
        public static readonly string PlacedByKey = "plumbers_placedBy";
        public static readonly string PlacedByPhoneNumberKey = "plumbers_placedByPhoneNumber";
        public static readonly string PlacedByIntegrationKey = "PlacedBy";
        public static readonly string ProductModelNumbersKey = "plumbers_modelNumbers";
        public static readonly string ProductKeywordsKey = "plumbers_productKeywords";
        public static readonly string SpecificationVisibleOnlyForSalesRepsKey = "plumbers_SpecificationSalesRepOnly";
        public static readonly string CustomerCanViewInventoryKey = "plumbers_canViewInventory";
        public static readonly string BillTrustUrlKey = "BillTrustUrl";
        public static readonly string PaymentMethodMaskText = "Payment_Method_Mask_Text";
        public static readonly string ApprovalTypeIntegrationKey = "approvty";
        public static readonly string ApprovalTypeKey = "plumbers_approvalType";
        public static readonly string PlumbersEmployeeKey = "plumbers_employee";
        public static readonly string PlumbersProductIsNotAvailableKey = "Product is Not Available";
        public static readonly string PlumbersOnAccountKey = "On Account";
        public static readonly string PlumbersCustomerSegmentKey = "plumbers_customerSegment";
        public static readonly string PlumbersWholesaleAvailability_InStockKey = "plumbers_wholeSaleInStock";
        public static readonly string PlumbersWholesaleAvailability_OutOfStockKey = "plumbers_wholeSaleOutOfStock";
        public static readonly string PlumbersWholesaleCustomerSegmentKey = "Wholesale";
    }
}