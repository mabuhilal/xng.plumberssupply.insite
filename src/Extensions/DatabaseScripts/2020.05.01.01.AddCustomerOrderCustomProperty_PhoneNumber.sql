﻿DECLARE @customerOrderEntityConfigId UNIQUEIDENTIFIER;
SET @customerOrderEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'customerOrder'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_placedByPhoneNumber'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@customerOrderEntityConfigId
				,'plumbers_placedByPhoneNumber'
				,null
				,'Placed By Phone Number'
				,'Insite.Admin.ControlTypes.TextFieldControl'
				,0
				,null
				,'Phone Number of who placed the order.'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Phone Number of who placed the order.'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO