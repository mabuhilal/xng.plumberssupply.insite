﻿-- Create property configuration record.
DECLARE @entityConfigId UNIQUEIDENTIFIER;
SET @entityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'userProfile'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_employee'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@entityConfigId
				,'plumbers_employee'
				,null
				,'Plumbers Supply Employee'
				,'Insite.Admin.ControlTypes.ToggleSwitchControl'
				,0
				,null
				,'Website user is a Plumbers employee.'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Website user is a Plumbers employee.'
				,'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO