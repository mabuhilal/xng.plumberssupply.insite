﻿DECLARE @productEntityConfigId UNIQUEIDENTIFIER;
SET @productEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'Product'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_ModelNumbers'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@productEntityConfigId
				,'plumbers_modelNumbers'
				,null
				,'Model Numbers'
				,'Insite.Admin.ControlTypes.TextFieldControl'
				,0
				,null
				,'Comma-separated product model numbers'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Comma-separated product model numbers'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO