﻿  delete from dbo.CustomProperty 
  where name = 'plumbers_SpecificationSalesRep'

  update AppDict.PropertyConfiguration
  set 
	Name = 'plumbers_SpecificationSalesRepOnly',
	Label = 'Visible only to sales reps',
	ControlType = 'Insite.Admin.ControlTypes.ToggleSwitchControl',
	PropertyType = 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089',
	HintText = 'Specification is visible only to sales reps',
	ToolTip = 'Specification is visible only to sales reps'
  where name = 'plumbers_SpecificationSalesRep'