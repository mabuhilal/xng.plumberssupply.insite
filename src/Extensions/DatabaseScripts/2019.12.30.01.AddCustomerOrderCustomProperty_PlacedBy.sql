﻿DECLARE @customerOrderEntityConfigId UNIQUEIDENTIFIER;
SET @customerOrderEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'customerOrder'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_placedBy'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@customerOrderEntityConfigId
				,'plumbers_placedBy'
				,null
				,'Placed By'
				,'Insite.Admin.ControlTypes.TextFieldControl'
				,0
				,null
				,'Free-form entry for who placed the order'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Free-form entry for who placed the order'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO