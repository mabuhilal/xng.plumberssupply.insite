﻿-- Create property configuration record.
DECLARE @customerEntityConfigId UNIQUEIDENTIFIER;
SET @customerEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'customer'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_customerSegment'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@customerEntityConfigId
				,'plumbers_customerSegment'
				,null
				,'Customer Segment'
				,'Insite.Admin.ControlTypes.DropDownControl'
				,0
				,null
				,'The segment this customer belongs to.'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'The segment this customer belongs to.'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO

-- Set property configuration record for the multiselect control.
DECLARE @customerPropertyConfigurationId UNIQUEIDENTIFIER;
SET @customerPropertyConfigurationId = (
	SELECT Id FROM AppDict.[PropertyConfiguration]
	WHERE name = 'plumbers_customerSegment'
)
IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyAttributeConfiguration]
	WHERE [Value] = 'Heating & HVAC, Jack of all Trades, Plumbing & Heating, Wholesale'
)
BEGIN
INSERT INTO [AppDict].[PropertyAttributeConfiguration]
           ([PropertyConfigurationId]
           ,[Name]
           ,[Value]
           ,[CreatedOn]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[ModifiedBy])
     VALUES
           (@customerPropertyConfigurationId
           ,'MultiValueList'
           ,'Heating & HVAC, Jack of all Trades, Plumbing & Heating, Wholesale'
           ,GETUTCDATE()
           ,'Script'
           ,GETUTCDATE()
           ,'')
END
GO