﻿DECLARE @entityConfigId UNIQUEIDENTIFIER;
SET @entityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'product'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_productKeywords'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@entityConfigId
				,'plumbers_productKeywords'
				,null
				,'Product Keywords'
				,'Insite.Admin.ControlTypes.TextFieldControl'
				,0
				,null
				,'Additional product search keywords'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Additional product search keywords'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO