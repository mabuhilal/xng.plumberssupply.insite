﻿update AppDict.PropertyConfiguration 
set 
	HintText = 'Determines whether a website user can view the employee resources tab for product details.', 
	ToolTip = 'Determines whether a website user can view the employee resources tab for product details.'
where name = 'plumbers_employee'
