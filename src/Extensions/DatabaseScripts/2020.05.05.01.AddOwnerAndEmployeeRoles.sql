﻿IF NOT EXISTS
(
	SELECT 1
	FROM dbo.AspNetRoles
	WHERE [Name] = 'Owner'
	OR [Name] = 'Employee'
)
BEGIN
	INSERT INTO [dbo].[AspNetRoles] (Id, [Name])
	VALUES (convert(nvarchar(128), NEWID()), 'Owner')

	INSERT INTO [dbo].[AspNetRoles] (Id, [Name])
	VALUES (convert(nvarchar(128), NEWID()), 'Employee')
END
GO