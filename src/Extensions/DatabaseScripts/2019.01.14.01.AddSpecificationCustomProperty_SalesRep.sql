﻿DECLARE @specificationEntityConfigId UNIQUEIDENTIFIER;
SET @specificationEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'specification'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_SpecificationSalesRepOnly'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@specificationEntityConfigId
				,'plumbers_SpecificationSalesRepOnly'
				,null
				,'Sales Rep Number'
				,'Insite.Admin.ControlTypes.TextFieldControl'
				,0
				,1
				,'The sales rep number for who can see this specification.'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'The sales rep number for who can see this specification.'
				,'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO