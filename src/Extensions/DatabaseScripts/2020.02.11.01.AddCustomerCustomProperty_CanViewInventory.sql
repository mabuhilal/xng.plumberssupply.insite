﻿
DECLARE @customerEntityConfigId UNIQUEIDENTIFIER;
SET @customerEntityConfigId = (
	SELECT Id FROM AppDict.EntityConfiguration
	WHERE name = 'customer'
)

IF NOT EXISTS
(
	SELECT 1
	FROM [AppDict].[PropertyConfiguration]
	WHERE [Name] = 'plumbers_canViewInventory'
)
BEGIN
	INSERT INTO [AppDict].[PropertyConfiguration]
				([EntityConfigurationId]
				,[Name]
				,[ParentProperty]
				,[Label]
				,[ControlType]
				,[IsRequired]
				,[IsTranslatable]
				,[HintText]
				,[CreatedOn]
				,[CreatedBy]
				,[ModifiedOn]
				,[ModifiedBy]
				,[ToolTip]
				,[PropertyType]
				,[IsCustomProperty]
				,[CanView]
				,[CanEdit]
				,[HintTextPropertyName])
			VALUES
				(@customerEntityConfigId
				,'plumbers_canViewInventory'
				,null
				,'Can View Inventory'
				,'Insite.Admin.ControlTypes.ToggleSwitchControl'
				,0
				,null
				,'Determines whether the customer can view inventory values.'
				,GETUTCDATE()
				,''
				,GETUTCDATE()
				,''
				,'Determines whether the customer can view inventory values.'
				,'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
				,1
				,1
				,1
				,null)
END
GO