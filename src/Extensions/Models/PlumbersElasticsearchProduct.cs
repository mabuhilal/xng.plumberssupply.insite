﻿using System.Collections.Generic;
using Insite.Search.Elasticsearch.DocumentTypes.Product;
using Extensions.Mapper;
using Nest;

namespace Extensions.Models
{
    [ElasticsearchType(Name = "product")]
    public class PlumbersElasticsearchProduct : ElasticsearchProduct
    {
        public PlumbersElasticsearchProduct()
        {
        }

        public PlumbersElasticsearchProduct(ElasticsearchProduct elasticsearchProduct)
        {
            PropertyCopier<ElasticsearchProduct, PlumbersElasticsearchProduct>.Copy(elasticsearchProduct, this);
        }

        [Keyword(Name = "plumbers_modelNumbers", Index = true)]
        public List<string> ModelNumbers { get; set; }

        [Keyword(Name = "plumbers_productKeywords", Index = true)]
        public List<string> ProductKeywords { get; set; }
    }
}