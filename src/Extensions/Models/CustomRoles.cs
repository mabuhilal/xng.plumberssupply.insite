﻿namespace Extensions.Models.CustomRoles
{
    public class CustomRoles
    {
        public const string Owner = "Owner";
        public const string Employee = "Employee";
    }
}