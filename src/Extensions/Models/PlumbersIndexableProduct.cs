﻿using Insite.Search.Elasticsearch.DocumentTypes.Product.Index;
namespace Extensions.Models
{
    public class PlumbersIndexableProduct : IndexableProduct
    {
        public string ModelNumbers { get; set; }
    }
}