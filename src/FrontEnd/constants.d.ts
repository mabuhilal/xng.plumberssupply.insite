/** Compile-time constant; when true, it's a production build, otherwise false. */
declare const IS_PRODUCTION: boolean;

/** Compile-time constant; when true, code will run server-side, otherwise false. */
declare const IS_SERVER_SIDE: boolean;
