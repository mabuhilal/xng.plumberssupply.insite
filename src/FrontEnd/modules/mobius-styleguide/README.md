# @insite/mobius-styleguide

Visual documentation and sandbox for the Mobius component library.

## Running styleguide locally

To run the styleguide locally, run the start script from the mobius-styleguide folder: 
```
npm run start
```
The styleguide will run by default on localhost:6060.

## Building and Deploying

This package is currently being deployed to the gh-pages branch of this repository.
To push a new build, run the deploy script from the mobius-styleguide folder:
```
npm run deploy
```
