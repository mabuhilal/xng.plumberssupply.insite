import * as React from "react";
import AddToListModal from "@insite/content-library/Components/AddToListModal";

const Modals: React.FC = () => {
    return <>
            <AddToListModal />
        </>;
};

export default Modals;
