# @insitesoft/mobius

Alpha release of Mobius, a collection of components to be used in building your web InsiteCommerce site.

## Importing Components

Once installed, @insitesoft/mobius components can be imported as any other JavaScript module:
```
import Button from '@insitesoft/mobius/Button';
```

## Styleguide and documentation

Documentation for the React-based components can be found at the [Mobius Styleguide](https://insitesoftware.github.io/insite-commerce/)