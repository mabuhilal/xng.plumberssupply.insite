import { ComponentThemeProps } from "../globals/baseTheme";

const OverflowMenuPresentationPropsDefault: ComponentThemeProps["overflowMenu"]["defaultProps"] = {
    buttonProps: {
        color: "common.background",
        shape: "pill",
        buttonType: "solid",
        shadow: false,
    },
};

export default OverflowMenuPresentationPropsDefault;
