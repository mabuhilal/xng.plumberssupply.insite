import { ComponentThemeProps } from "../globals/baseTheme";

const BreadcrumbsPresentationPropsDefault: ComponentThemeProps["breadcrumbs"]["defaultProps"] = {
    typographyProps: {
        size: 13,
    },
};

export default BreadcrumbsPresentationPropsDefault;
