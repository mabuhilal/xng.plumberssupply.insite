import { ComponentThemeProps } from "../globals/baseTheme";

const IconPresentationPropsDefault: ComponentThemeProps["icon"]["defaultProps"] = {
    color: "text.main",
    size: 24,
};

export default IconPresentationPropsDefault;
