import { ComponentThemeProps } from "../globals/baseTheme";

const LinkPresentationPropsDefault: ComponentThemeProps["link"]["defaultProps"] = {
    color: "text.link",
    hoverMode: "darken",
};

export default LinkPresentationPropsDefault;
