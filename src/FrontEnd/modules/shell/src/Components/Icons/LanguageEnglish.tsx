import * as React from 'react';

const LanguageEnglish: React.FC<{ size?: number, color1?: string, color2?: string, color3?: string, color4?: string, className?: string }> = ({ size = 21, color1 = '#FFF', color2 = '#E4171E', color3 = '#FCFCFC', color4 = '#0055A5', className }) => (
<svg focusable="false" className={className} viewBox="0 0 21 21" width={size} height={size} enableBackground="new 0 0 21 21">
    <defs>
        <filter id="a" filterUnits="userSpaceOnUse" x="-4.9" y="18.9" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="18.9" width="30.2" height="1.6" id="b">
        <g filter="url(#a)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#b)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 20.5h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="c" filterUnits="userSpaceOnUse" x="-4.9" y="17.3" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="17.3" width="30.2" height="1.6" id="d">
        <g filter="url(#c)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#d)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 18.9h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="e" filterUnits="userSpaceOnUse" x="-4.9" y="15.8" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="15.8" width="30.2" height="1.6" id="f">
        <g filter="url(#e)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#f)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 17.4h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="g" filterUnits="userSpaceOnUse" x="-4.9" y="14.2" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="14.2" width="30.2" height="1.6" id="h">
        <g filter="url(#g)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#h)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 15.8h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="i" filterUnits="userSpaceOnUse" x="-4.9" y="12.6" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="12.6" width="30.2" height="1.6" id="j">
        <g filter="url(#i)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#j)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 14.2h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="k" filterUnits="userSpaceOnUse" x="-4.9" y="11.1" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="11.1" width="30.2" height="1.6" id="l">
        <g filter="url(#k)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#l)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 12.7h30.2v-1.6H-4.9v1.6z"/>
    <defs>
        <filter id="m" filterUnits="userSpaceOnUse" x="-4.9" y="9.5" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="9.5" width="30.2" height="1.6" id="n">
        <g filter="url(#m)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#n)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 11.1h30.2V9.5H-4.9v1.6z"/>
    <defs>
        <filter id="o" filterUnits="userSpaceOnUse" x="-4.9" y="8" width="30.2" height="1.5">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="8" width="30.2" height="1.5" id="p">
        <g filter="url(#o)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#p)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 9.5h30.2V8H-4.9v1.5z"/>
    <defs>
        <filter id="q" filterUnits="userSpaceOnUse" x="-4.9" y="6.4" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="6.4" width="30.2" height="1.6" id="r">
        <g filter="url(#q)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#r)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 8h30.2V6.4H-4.9V8z"/>
    <defs>
        <filter id="s" filterUnits="userSpaceOnUse" x="-4.9" y="4.8" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="4.8" width="30.2" height="1.6" id="t">
        <g filter="url(#s)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#t)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 6.4h30.2V4.8H-4.9v1.6z"/>
    <defs>
        <filter id="u" filterUnits="userSpaceOnUse" x="-4.9" y="3.3" width="30.2" height="1.5">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="3.3" width="30.2" height="1.5" id="v">
        <g filter="url(#u)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#v)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 4.8h30.2V3.3H-4.9v1.5z"/>
    <defs>
        <filter id="w" filterUnits="userSpaceOnUse" x="-4.9" y="1.7" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y="1.7" width="30.2" height="1.6" id="x">
        <g filter="url(#w)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#x)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M-4.9 3.3h30.2V1.7H-4.9v1.6z"/>
    <defs>
        <filter id="y" filterUnits="userSpaceOnUse" x="-4.9" y=".1" width="30.2" height="1.6">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y=".1" width="30.2" height="1.6" id="z">
        <g filter="url(#y)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#z)" fillRule="evenodd" clipRule="evenodd" fill={color2} d="M-4.9 1.7h30.2V.1H-4.9v1.6z"/>
    <defs>
        <filter id="A" filterUnits="userSpaceOnUse" x="-4.9" y=".1" width="15" height="11">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-4.9" y=".1" width="15" height="11" id="B">
        <g filter="url(#A)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#B)" fillRule="evenodd" clipRule="evenodd" fill={color4} d="M-4.9 11.1h15V.1h-15v11z"/>
    <defs>
        <filter id="C" filterUnits="userSpaceOnUse" x="-.3" y=".6" width="9.7" height="9.9">
            <feColorMatrix values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"/>
        </filter>
    </defs>
    <mask maskUnits="userSpaceOnUse" x="-.3" y=".6" width="9.7" height="9.9" id="D">
        <g filter="url(#C)">
            <path fillRule="evenodd" clipRule="evenodd" fill={color1} d="M10.3.1C4.7.1.2 4.7.2 10.3s4.5 10.2 10.1 10.2 10.1-4.6 10.1-10.2S15.8.1 10.3.1z"/>
        </g>
    </mask>
    <path mask="url(#D)" fillRule="evenodd" clipRule="evenodd" fill={color3} d="M1.5 1L1.4.6l-.1.3V1H.9l.3.2-.1.5.3-.2.3.2-.1-.4.3-.3h-.4zM4 1L3.9.6l-.1.3V1h-.5l.3.2v.5l.3-.2.3.2-.1-.4.3-.3H4zm2.5 0L6.4.6l-.1.3V1h-.5l.3.2-.1.5.3-.2.3.2v-.4l.3-.3h-.4zM9 1L8.9.6l-.1.3V1h-.5l.3.2-.1.5.3-.2.3.2v-.4l.3-.3H9zM.3 2.1l-.1-.4-.1.3v.1h-.4l.3.2-.1.4.3-.2.3.2-.1-.3.3-.2-.4-.1zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.5.3-.2.4.2-.1-.4.3-.2-.4-.1zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.5.3-.2.3.2v-.4l.3-.2-.4-.1zm2.5 0l-.2-.4-.1.3v.1h-.4l.3.2-.1.5.3-.2.4.2-.2-.4.3-.2-.3-.1zM1.5 3.3l-.1-.4-.1.2v.1H.9l.3.2-.1.5.3-.2.3.2-.1-.4.3-.2h-.4zm2.5 0l-.1-.4-.1.2v.1h-.5l.3.2v.5l.3-.2.3.2-.1-.4.3-.2H4zm2.5 0l-.1-.4-.1.2v.1h-.5l.3.2v.5l.3-.2.3.2-.1-.4.3-.2h-.4zm2.5 0l-.1-.4-.1.2v.1h-.5l.3.2-.1.5.3-.2.3.2v-.4l.3-.2H9zM.3 4.4L.2 4l-.1.2v.1h-.4l.3.2-.2.5.3-.2.4.2-.1-.4.3-.2H.3zm2.5 0L2.6 4v.3h-.5l.3.2-.1.5.3-.2.4.2-.1-.4.3-.2h-.4zm2.5 0L5.1 4v.3h-.5l.3.2-.1.5.3-.2.4.2-.1-.4.3-.2h-.4zm2.5 0L7.6 4l-.1.2v.1h-.4l.3.2-.1.5.3-.2.4.2-.2-.4.3-.2h-.3zM1.5 5.5l-.1-.4-.1.3v.1H.9l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2h-.4zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2v.4l.3-.2.3.2-.1-.4.3-.2H4zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2v.4l.3-.2.3.2-.1-.4.3-.2h-.4zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2-.1.4.3-.2.3.2v-.4l.3-.2H9zM.3 6.6l-.1-.4-.1.3v.1h-.4l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2H.3zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.4.3-.2.4.2-.1-.4.3-.2h-.4zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.4.3-.2.3.2v-.4l.3-.2h-.4zm2.5 0l-.2-.4-.1.3v.1h-.4l.3.2-.1.4.3-.2.4.2-.2-.4.3-.2h-.3zM1.5 7.7l-.1-.4-.1.3v.1H.9l.3.3-.1.4.3-.2.3.2-.1-.4.3-.2h-.4v-.1zm2.5 0l-.1-.4-.1.3v.1h-.5l.4.3-.1.4.3-.2.3.2-.1-.4.3-.2H4v-.1zm2.5 0l-.1-.4-.1.3v.1h-.5l.4.3-.1.4.3-.2.3.2-.1-.4.3-.2h-.4v-.1zm2.5 0l-.1-.4-.1.3v.1h-.5l.4.3-.2.4.3-.2.3.2V8l.3-.2H9v-.1zM.3 8.8l-.1-.4-.1.3v.1h-.4L0 9l-.1.4.3-.2.3.2-.1-.3.3-.2H.3v-.1zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.5.3-.2.4.2-.1-.4.3-.2h-.4v-.1zm2.5 0l-.2-.4v.4h-.5l.3.2-.1.5.3-.2.3.2v-.4l.3-.2h-.4v-.1zm2.5 0l-.2-.4-.1.3v.1h-.4l.3.2-.1.5.3-.2.4.2-.2-.4.3-.2h-.3v-.1zM1.5 9.9l-.1-.4-.1.3v.1H.9l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2h-.4zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2H4zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2h-.3zm2.5 0l-.1-.4-.1.3v.1h-.5l.3.2-.1.4.3-.2.3.2-.1-.4.3-.2H9z"/>
</svg>
);

export default React.memo(LanguageEnglish);
