import { DataViewState } from "@insite/client-framework/Store/Data/DataState";
import { Cart } from "@insite/client-framework/Services/CartService";

export interface CartsState extends DataViewState<Cart> {
}
