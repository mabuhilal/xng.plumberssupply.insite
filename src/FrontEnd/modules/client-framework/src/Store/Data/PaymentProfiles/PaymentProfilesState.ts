import { AccountPaymentProfileModel } from "@insite/client-framework/Types/ApiModels";
import { DataViewState } from "@insite/client-framework/Store/Data/DataState";

export interface PaymentProfilesState extends DataViewState<AccountPaymentProfileModel> {
}
