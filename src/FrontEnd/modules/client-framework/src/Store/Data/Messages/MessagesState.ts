import { MessageModel } from "@insite/client-framework/Types/ApiModels";
import { DataViewState } from "@insite/client-framework/Store/Data/DataState";

export interface MessagesState extends DataViewState<MessageModel> {
}
