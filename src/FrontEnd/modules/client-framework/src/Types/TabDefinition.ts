export interface TabDefinition {
    displayName: string;
    sortOrder: number;
}
