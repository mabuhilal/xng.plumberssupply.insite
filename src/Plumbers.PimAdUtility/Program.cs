﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using PlumbersCategoryListGenerator.Models;

using OfficeOpenXml;

namespace ProductImportGenerator
{
    class Program
    {
        const string CATEGORY_NAME_KEY = "CATEGORY_NAME";
        const string CATEGORY_NAME_LEVEL1_KEY = "CATEGORY_NAME_1_LEVEL_1";
        const string ATTRIBUTE_NAME_KEY = "ATTRIBUTE_NAME_";
        const string ATTRIBUTE_VALUE_KEY = "ATTRIBUTE_VALUE_";
        const string ITEM_FEATURES_KEY = "ITEM_FEATURES_";
        const string MY_PART_NUMBER_KEY = "MY_PART_NUMBER";
        const string ITEM_IMAGE_KEY = "ITEM_IMAGE_ITEMIMAGE";
        const string ITEM_DOCUMENT_NAME_KEY = "ITEM_DOCUMENT_NAME_";
        const string ITEM_DOCUMENT_TYPE_KEY = "ITEM_DOCUMENT_TYPE_";
        const string ITEM_DOCUMENT_CAPTION_KEY = "DOC_CAPTION_";
        const string CALCULATED_URL_SEGMENT_KEY = "Calculated_Url_Segment";
        const string CALCULATED_SPECIFICATION_FEATURES_KEY = "Calculated_Specification_Features";

        private static string readPathExcel = string.Empty;

        static void Main(string[] args)
        {
            Console.WriteLine($"Start time {DateTime.Now.ToString()}");
            Console.WriteLine("");
            Console.WriteLine("------------------------------");
            
            while (PromptForValidInputPath() != true)
            {
            }

            var readPathExcelCategoryProductTemplate = $@"App_Data/PSC_AD_PIM_CategoryAssignment_Template.xlsx";
            var readPathExcelProductAssetTemplate = $@"App_Data/AssetProductAssignmentTemplate.xlsx";
            var readPathExcelProductAssetImportTemplate = $@"App_Data/ExternalAssetImportTemplate.xlsx";

            var writePathExcelAdProductImport = @"App_Data/FileOutput/PSC_AD_Product_Import.xlsx";
            var writePathExcelCategoryProductImport = @"App_Data/FileOutput/PSC_AD_Category_Product_Import.xlsx";
            var writePathExcelProductAssetAssignment = @"App_Data/FileOutput/PSC_AD_Product_Asset_Assignment.xlsx";
            var writePathExcelProductAssetImport = @"App_Data/FileOutput/PSC_AD_Product_Asset_Import.xlsx";

            var headerDict = new Dictionary<string, int>();

            using (var package = new ExcelPackage(new FileInfo(readPathExcel)))
            {
                using (var packageCategoryProduct = new ExcelPackage(new FileInfo(readPathExcelCategoryProductTemplate)))
                {
                    using (var packageProductAsset = new ExcelPackage(new FileInfo(readPathExcelProductAssetTemplate)))
                    {
                        using (var packageProductAssetImport = new ExcelPackage(new FileInfo(readPathExcelProductAssetImportTemplate)))
                        {
                            var workSheetAdSource = package.Workbook.Worksheets[0];
                            var workSheetCategoryProduct = packageCategoryProduct.Workbook.Worksheets[0];
                            var workSheetProductAsset = packageProductAsset.Workbook.Worksheets[0];
                            var workSheetProductAssetImport = packageProductAssetImport.Workbook.Worksheets[0];

                            // Set headers for later index lookup
                            BuildHeadersDictionary(headerDict, workSheetAdSource);

                            // Iterate rows. Skip header row.
                            for (int rowIndex = 2; rowIndex <= workSheetAdSource.Dimension.Rows; rowIndex++)
                            {
                                Console.WriteLine($"Processing row {rowIndex}..");

                                var rowIndexesAndValues = new Dictionary<int, string>();

                                for (int columnIndex = 1; columnIndex < workSheetAdSource.Dimension.Columns; columnIndex++)
                                {
                                    var currentHeader = headerDict.Where(o => o.Value == columnIndex).FirstOrDefault();
                                    var isAttribute = false;
                                    var attributeContainsValue = false;

                                    if (!string.IsNullOrEmpty(currentHeader.Key) && currentHeader.Key.Contains(ATTRIBUTE_NAME_KEY))
                                    {
                                        var attributeValue = workSheetAdSource.Cells[rowIndex, columnIndex + 1].Text;
                                        isAttribute = true;
                                        attributeContainsValue = !string.IsNullOrEmpty(attributeValue);
                                    }

                                    var currentFieldVal = workSheetAdSource.Cells[rowIndex, columnIndex].Text;

                                    // HTML decode fields.
                                    if (currentFieldVal.Contains("&"))
                                    {
                                        currentFieldVal = currentFieldVal
                                            .Replace("&reg;", HttpUtility.HtmlDecode("&reg;"))
                                            .Replace("&trade;", HttpUtility.HtmlDecode("&trade;"))
                                            .Replace("&lt;", HttpUtility.HtmlDecode("&lt;"))
                                            .Replace("&", "&amp;");
                                    }

                                    workSheetAdSource.Cells[rowIndex, columnIndex].Value = HttpUtility.HtmlDecode(currentFieldVal);

                                    var currentCell = workSheetAdSource.Cells[rowIndex, columnIndex].Text;

                                    if (!string.IsNullOrEmpty(currentCell))
                                    {
                                        if (isAttribute == true && attributeContainsValue == false)
                                        {
                                            continue;
                                        }

                                        rowIndexesAndValues[columnIndex] = currentCell;
                                    }
                                }

                                // Product import processing.
                                BuildCalculatedUrlSegmentFields(workSheetAdSource, rowIndex, rowIndexesAndValues, headerDict);
                                BuildProductTemplateFields(workSheetAdSource, rowIndex, rowIndexesAndValues, headerDict);
                                BuildCalculatedItemFeatureFields(workSheetAdSource, rowIndex, rowIndexesAndValues, headerDict);
                                BuildCalculatedAttributesFields(workSheetAdSource, rowIndex, rowIndexesAndValues, headerDict);

                                // Category product mapping file processing.
                                BuildCategoryProductImportFields(workSheetAdSource, workSheetCategoryProduct, rowIndex, rowIndexesAndValues, headerDict);

                                // Product asset import file processing.
                                BuildProductAssetAssigmentFields(workSheetAdSource, workSheetProductAsset, rowIndex, rowIndexesAndValues, headerDict);

                                // Product asset import file processing.
                                BuildProductAssetImportFields(workSheetAdSource, workSheetProductAssetImport, rowIndex, rowIndexesAndValues, headerDict);
                            }

                            // Create output directory if not existing.
                            if (!Directory.Exists(@"App_Data/FileOutput/"))
                            {
                                Directory.CreateDirectory(@"App_Data/FileOutput/");
                            }

                            // Save output xslx file for AD product import
                            var fileInfo = new FileInfo(writePathExcelAdProductImport);
                            package.SaveAs(fileInfo);

                            // Save another output xslx file for category / product mapping import 
                            var fileInfoCategoryProductMap = new FileInfo(writePathExcelCategoryProductImport);
                            packageCategoryProduct.SaveAs(fileInfoCategoryProductMap);

                            // Save another output xslx file for category / product mapping import 
                            var fileInfoProductAsset = new FileInfo(writePathExcelProductAssetAssignment);
                            packageProductAsset.SaveAs(fileInfoProductAsset);

                            // Save another output xslx file for the product asset import template. 
                            var fileInfoProductAssetImport = new FileInfo(writePathExcelProductAssetImport);
                            packageProductAssetImport.SaveAs(fileInfoProductAssetImport);

                            Console.WriteLine($"End {DateTime.Now.ToString()}");
                        }
                    }
                }
            }
        }

        private static bool PromptForValidInputPath()
        {
            Console.WriteLine("");
            Console.WriteLine("> Enter the path to the AD source data file:");

            var enteredPath = Console.ReadLine();

            Console.WriteLine($"> Is this the correct path? (Y/N): {enteredPath}");

            var isPathCorrectVal = Console.ReadLine();
            var isPathCorrect = !string.IsNullOrEmpty(isPathCorrectVal) && isPathCorrectVal.ToLower() == "y";
            readPathExcel = enteredPath;
            return isPathCorrect;
        }

        private static void BuildHeadersDictionary(Dictionary<string, int> headerDict, ExcelWorksheet workSheet)
        {
            for (int columnIndex = 1; columnIndex < workSheet.Dimension.Columns; columnIndex++)
            {
                var currentCell = workSheet.Cells[1, columnIndex].Text ?? string.Empty;

                if (currentCell.Contains(CATEGORY_NAME_KEY)
                    || currentCell.Contains(ATTRIBUTE_NAME_KEY)
                    || currentCell.Contains(ATTRIBUTE_VALUE_KEY)
                    || currentCell.Contains(ITEM_FEATURES_KEY)
                    || currentCell.Contains(MY_PART_NUMBER_KEY)
                    || currentCell.Contains(CATEGORY_NAME_LEVEL1_KEY)
                    || currentCell.Contains(ITEM_IMAGE_KEY)
                    || currentCell.Contains(ITEM_DOCUMENT_NAME_KEY)
                    || currentCell.Contains(ITEM_DOCUMENT_TYPE_KEY)
                    || currentCell.Contains(ITEM_DOCUMENT_CAPTION_KEY))
                {
                    headerDict[currentCell] = columnIndex;
                }
            }
        }

        private static void BuildCalculatedUrlSegmentFields(
            ExcelWorksheet workSheet,
            int currentRowIndex,
            Dictionary<int, string> currentFieldData,
            Dictionary<string, int> headerDict)
        {
            foreach (var fieldKvp in currentFieldData)
            {
                if (headerDict[MY_PART_NUMBER_KEY] == fieldKvp.Key)
                {
                    AddAdditionalFieldData(workSheet, currentRowIndex, CALCULATED_URL_SEGMENT_KEY, fieldKvp.Value);
                }
            }
        }

        private static void AddAdditionalFieldData(ExcelWorksheet workSheet, int currentRowIndex, string targetAdditionalField, string valueToAdd)
        {
            var columnAlreadyExists = false;
            var targetColumnIndex = -1;

            // Set column exists if found in headers.
            for (int columnIndex = 1; columnIndex < workSheet.Dimension.Columns + 1; columnIndex++)
            {
                var currentHeaderName = workSheet.Cells[1, columnIndex].Text ?? string.Empty;

                if (currentHeaderName == targetAdditionalField)
                {
                    columnAlreadyExists = true;
                    targetColumnIndex = columnIndex;
                }
            }

            // Add column if not found.
            if (!columnAlreadyExists)
            {
                targetColumnIndex = workSheet.Dimension.Columns + 1;
                workSheet.InsertColumn(targetColumnIndex, 1);
                workSheet.Cells[1, targetColumnIndex].Value = targetAdditionalField;
            }

            // Set row value for new column.
            workSheet.Cells[currentRowIndex, targetColumnIndex].Value = valueToAdd;
        }

        private static void AddAdditionalFieldDataCategoryProductMap(ExcelWorksheet workSheet, CategoryDTO categoryDto)
        {
            workSheet.Cells[categoryDto.RowCount, 1].Value = "Plumbers Supply";
            workSheet.Cells[categoryDto.RowCount, (categoryDto.CategoryLevel + 1)].Value = categoryDto.RowData;
            workSheet.Cells[categoryDto.RowCount, 8].Value = categoryDto.ProductNumber;
        }

        private static void BuildProductAssetImportFields(ExcelWorksheet workSheetAdSource, ExcelWorksheet workSheetProductAssetImport, int rowIndex, Dictionary<int, string> currentFieldData, Dictionary<string, int> headerDict)
        {
            var targetFieldData = new Dictionary<string, string>();

            // Build rows for image and document fields.
            foreach (var fieldKvp in currentFieldData.Where(o => !string.IsNullOrEmpty(o.Value)))
            {
                if (headerDict.Where(h => h.Value == fieldKvp.Key
                    && (h.Key.Contains(ITEM_IMAGE_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_NAME_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_TYPE_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_CAPTION_KEY)
                        || h.Key.Contains(MY_PART_NUMBER_KEY)))
                    .Any())
                {
                    var fieldName = headerDict.Where(h => h.Value == fieldKvp.Key).FirstOrDefault();
                    targetFieldData[fieldName.Key] = fieldKvp.Value;
                }
            }

            CreateProductAssetImportRow(workSheetProductAssetImport, targetFieldData, rowIndex);
        }

        private static void BuildProductAssetAssigmentFields(ExcelWorksheet workSheetAdSource, ExcelWorksheet workSheetProductAsset, int rowIndex, Dictionary<int, string> currentFieldData, Dictionary<string, int> headerDict)
        {
            var targetFieldData = new Dictionary<string, string>();

            // Build rows for image and document fields.
            foreach (var fieldKvp in currentFieldData.Where(o => !string.IsNullOrEmpty(o.Value)))
            {
                if (headerDict.Where(h => h.Value == fieldKvp.Key
                    && (h.Key.Contains(ITEM_IMAGE_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_NAME_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_TYPE_KEY)
                        || h.Key.Contains(ITEM_DOCUMENT_CAPTION_KEY)
                        || h.Key.Contains(MY_PART_NUMBER_KEY)))
                    .Any())
                {
                    var fieldName = headerDict.Where(h => h.Value == fieldKvp.Key).FirstOrDefault();
                    targetFieldData[fieldName.Key] = fieldKvp.Value;
                }
            }

            CreateProductAssetRow(workSheetProductAsset, targetFieldData, rowIndex);
        }

        private static void CreateProductAssetImportRow(ExcelWorksheet workSheetProductAssetImport, Dictionary<string, string> targetFieldData, int rowIndex)
        {
            var assetNameIndex = 1;
            var assetTypeIndex = 2;
            var otherAssetUrlIndex = 8;

            foreach (var kvp in targetFieldData.Where(
                h => h.Key.Contains(ITEM_DOCUMENT_NAME_KEY)))
            {
                var currentKey = kvp.Key.Substring(kvp.Key.Length - 1, 1);
                int.TryParse(currentKey,
                    out int docColumnNumber);

                if (!targetFieldData.ContainsKey(ITEM_DOCUMENT_NAME_KEY + docColumnNumber))
                {
                    continue;
                }

                var currentRow = workSheetProductAssetImport.Dimension.Rows + 1;
                var isUrlDocType = kvp.Key.Contains(ITEM_DOCUMENT_NAME_KEY)
                    && targetFieldData[ITEM_DOCUMENT_TYPE_KEY + docColumnNumber] == "URL";

                var urlCalculatedAssetName = string.Empty;
                if (isUrlDocType)
                {
                    urlCalculatedAssetName = $"{targetFieldData[MY_PART_NUMBER_KEY]} {targetFieldData[ITEM_DOCUMENT_CAPTION_KEY + docColumnNumber]} {docColumnNumber}";

                    workSheetProductAssetImport.Cells[currentRow, assetNameIndex].Value = urlCalculatedAssetName;
                    workSheetProductAssetImport.Cells[currentRow, assetTypeIndex].Value = "Other Asset";
                    workSheetProductAssetImport.Cells[currentRow, otherAssetUrlIndex].Value = targetFieldData[ITEM_DOCUMENT_NAME_KEY + docColumnNumber];
                }
            }
        }

        private static void CreateProductAssetRow(ExcelWorksheet workSheetProductAsset, Dictionary<string, string> targetFieldData, int rowIndex)
        {
            var folderFieldIndex = 1;
            var assetNameFieldIndex = 2;
            var productNumberFieldIndex = 3;

            // Handle image data fields.
            foreach (var kvp in targetFieldData.Where(h => h.Key.Contains(ITEM_IMAGE_KEY)))
            {
                var currentRow = workSheetProductAsset.Dimension.Rows + 1;

                workSheetProductAsset.Cells[currentRow, folderFieldIndex].Value = "Images";
                workSheetProductAsset.Cells[currentRow, assetNameFieldIndex].Value = (kvp.Value.Split(".")[0]);
                workSheetProductAsset.Cells[currentRow, productNumberFieldIndex].Value = targetFieldData[MY_PART_NUMBER_KEY];
            }

            // Handle document data fields.
            foreach (var kvp in targetFieldData.Where(
                h => h.Key.Contains(ITEM_DOCUMENT_NAME_KEY)))
            {
                var currentKey = kvp.Key.Substring(kvp.Key.Length - 1, 1);
                int.TryParse(currentKey,
                    out int docColumnNumber);

                if (!targetFieldData.ContainsKey(ITEM_DOCUMENT_NAME_KEY + docColumnNumber))
                {
                    continue;
                }

                var assetFolder = string.Empty;
                var assetName = string.Empty;
                var productNumber = string.Empty;
                var currentRow = workSheetProductAsset.Dimension.Rows + 1;
                var isUrlDocType = kvp.Key.Contains(ITEM_DOCUMENT_NAME_KEY)
                    && targetFieldData[ITEM_DOCUMENT_TYPE_KEY + docColumnNumber] == "URL";

                var urlCalculatedAssetName = string.Empty;
                if (isUrlDocType)
                {
                    urlCalculatedAssetName = $"{targetFieldData[MY_PART_NUMBER_KEY]} {targetFieldData[ITEM_DOCUMENT_CAPTION_KEY + docColumnNumber]} {docColumnNumber}";
                }

                // Set cell data on respective fields.
                workSheetProductAsset.Cells[currentRow, folderFieldIndex].Value = isUrlDocType ? "URLs" : "PDFs";
                workSheetProductAsset.Cells[currentRow, assetNameFieldIndex].Value = isUrlDocType
                    ? $"{urlCalculatedAssetName}"
                    : (targetFieldData[ITEM_DOCUMENT_NAME_KEY + docColumnNumber].Split(".")[0]);
                workSheetProductAsset.Cells[currentRow, productNumberFieldIndex].Value = targetFieldData[MY_PART_NUMBER_KEY];
            }
        }

        public static void BuildCalculatedAttributesFields(
            ExcelWorksheet workSheet,
            int currentRowIndex,
            Dictionary<int, string> currentFieldData,
            Dictionary<string, int> headerDict)
        {
            foreach (var fieldKvp in currentFieldData.Where(o => !string.IsNullOrEmpty(o.Value)))
            {
                if (headerDict.Where(h => h.Value == fieldKvp.Key && h.Key.Contains(ATTRIBUTE_NAME_KEY)).Any())
                {
                    var fieldName = headerDict.Where(h => h.Value == fieldKvp.Key).FirstOrDefault();
                    var catNameLevel1 = currentFieldData[headerDict[CATEGORY_NAME_LEVEL1_KEY]];
                    var categoryName = currentFieldData[headerDict[CATEGORY_NAME_KEY]];
                    var attrLevel = fieldName.Key.Split(ATTRIBUTE_NAME_KEY)[1];
                    var calculatedFieldName = $"Calculated_Name_{attrLevel}";

                    var calculatedAttrValue = ($"{catNameLevel1}{categoryName}{fieldKvp.Value}Attr")
                        .Replace(" ", "")
                        .Replace("/", "")
                        .Replace("&", "")
                        .Replace(",", "")
                        .Replace("-", "")
                        .Replace("(", "")
                        .Replace(")", "")
                        .Replace(".", "")
                        .Replace("@", "");

                    if (!string.IsNullOrEmpty(calculatedAttrValue))
                    {
                        AddAdditionalFieldData(workSheet, currentRowIndex, calculatedFieldName, calculatedAttrValue);
                    }
                }
            }
        }

        public static void BuildProductTemplateFields(
            ExcelWorksheet workSheet,
            int currentRowIndex,
            Dictionary<int, string> currentFieldData,
            Dictionary<string, int> headerDict)
        {
            foreach (var fieldKvp in currentFieldData)
            {
                if (headerDict.Where(h => h.Value == fieldKvp.Key).Any())
                {
                    var fieldName = headerDict.Where(h => h.Value == fieldKvp.Key).FirstOrDefault();
                    var catNameLevel1 = currentFieldData[headerDict[CATEGORY_NAME_LEVEL1_KEY]];
                    var categoryName = currentFieldData[headerDict[CATEGORY_NAME_KEY]];
                    var calculatedProductTemplateName = $"Calculated_Product_Template";

                    var calculatedProductTemplateValue = !string.IsNullOrEmpty(fieldKvp.Value)
                        ? ($"{catNameLevel1} {categoryName}")
                        : string.Empty;

                    if (!string.IsNullOrEmpty(calculatedProductTemplateValue))
                    {
                        AddAdditionalFieldData(workSheet, currentRowIndex, calculatedProductTemplateName, calculatedProductTemplateValue);
                    }
                }
            }
        }

        public static void BuildCalculatedItemFeatureFields(
            ExcelWorksheet workSheet,
            int currentRowIndex,
            Dictionary<int, string> currentFieldData,
            Dictionary<string, int> headerDict)
        {
            var itemsHtmlList = new StringBuilder();

            itemsHtmlList.Append(@"<ul>");

            foreach (var fieldKvp in currentFieldData)
            {
                foreach (var itemFeaturesFieldName in headerDict.Keys.Where(k => k.Contains(ITEM_FEATURES_KEY)))
                {
                    if (headerDict[itemFeaturesFieldName] == fieldKvp.Key)
                    {
                        itemsHtmlList.Append($"<li>{fieldKvp.Value}</li>");
                    }
                }
            }

            itemsHtmlList.Append(@"</ul>");

            var lineItemsAvailable = itemsHtmlList.ToString() != @"<ul></ul>";

            if (lineItemsAvailable)
            {
                AddAdditionalFieldData(workSheet, currentRowIndex, CALCULATED_SPECIFICATION_FEATURES_KEY, itemsHtmlList.ToString());
            }
        }

        public static void BuildCategoryProductImportFields(
            ExcelWorksheet workSheet,
            ExcelWorksheet workSheetCategoryProduct,
            int currentRowIndex,
            Dictionary<int, string> currentFieldData,
            Dictionary<string, int> headerDict)
        {
            foreach (var fieldKvp in currentFieldData)
            {
                if (headerDict.Where(h => h.Value == fieldKvp.Key && h.Key.Contains("CATEGORY_NAME_")).Any())
                {
                    try
                    {
                        var currentHeader = headerDict.Where(h => h.Value == fieldKvp.Key).FirstOrDefault();
                        var fieldData = fieldKvp.Value;
                        var categoryNameParts = currentHeader.Key.Split("CATEGORY_NAME_1_LEVEL_");
                        var productNumber = currentFieldData[headerDict[MY_PART_NUMBER_KEY]];
                        var catNameLevel1 = currentFieldData[headerDict[CATEGORY_NAME_LEVEL1_KEY]];

                        int.TryParse(categoryNameParts[1],
                            out int categoryLevel
                        );

                        var categoryDto = new CategoryDTO
                        {
                            CategoryLevel = categoryLevel,
                            CategoryName = $"Category {categoryNameParts[1]}",
                            ColumnNumber = fieldKvp.Key,
                            RowCount = currentRowIndex,
                            RowData = fieldData,
                            ProductNumber = productNumber
                        };

                        AddAdditionalFieldDataCategoryProductMap(workSheetCategoryProduct, categoryDto);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }
}
