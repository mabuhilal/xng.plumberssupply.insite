﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumbersCategoryListGenerator.Models
{
    public class CategoryDTO
    {
        public string CategoryName { get; set; }
        public int CategoryLevel { get; set; }
        public int RowCount { get; set; }
        public int ColumnNumber { get; set; }
        public string RowData { get; set; }
        public string ProductNumber { get; set; }
    }
}
