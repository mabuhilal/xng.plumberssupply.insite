﻿module insite.order {
    "use strict";

    export class PlumbersRecentlyPurchasedController extends RecentlyPurchasedController {
        canSeePrices: boolean;

        static $inject = ["settingsService", "productService", "cartService", "$scope", "sessionService"];

        constructor(
            protected settingsService: core.ISettingsService,
            protected productService: catalog.IProductService,
            protected cartService: cart.ICartService,
            protected $scope: ng.IScope,
            protected sessionService: account.ISessionService) {
            super(settingsService, productService, cartService, $scope);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("RecentlyPurchasedController", RecentlyPurchasedController);
}