﻿module insite.catalog {
    "use strict";

    export class PlumbersCrossSellCarouselController extends CrossSellCarouselController {
        canSeePrices: boolean;

        static $inject = ["cartService", "productService", "$timeout", "addToWishlistPopupService", "settingsService", "$scope", "sessionService"];

        constructor(
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected $timeout: ng.ITimeoutService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected settingsService: core.ISettingsService,
            protected $scope: ng.IScope,
            protected sessionService: account.ISessionService) {
            super(cartService, productService, $timeout, addToWishlistPopupService, settingsService, $scope);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("CrossSellCarouselController", CrossSellCarouselController);
}