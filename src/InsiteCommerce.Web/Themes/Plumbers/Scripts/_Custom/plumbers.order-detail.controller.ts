﻿module insite.order {
    "use strict";

    export class PlumbersOrderDetailController extends OrderDetailController {
        canSeePrices: boolean;

        protected getSessionCompleted(sessionModel: SessionModel) {
            super.getSessionCompleted(sessionModel);
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }
    }

    angular
        .module("insite")
        .controller("OrderDetailController", PlumbersOrderDetailController);
}