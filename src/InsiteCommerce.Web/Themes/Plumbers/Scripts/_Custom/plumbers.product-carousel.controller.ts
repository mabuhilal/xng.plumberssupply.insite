﻿module insite.catalog {

    export class PlumbersProductCarouselController extends ProductCarouselController {
        canSeePrices: boolean;

        static $inject = [
            "cartService",
            "productService",
            "$timeout",
            "addToWishlistPopupService",
            "settingsService",
            "$scope",
            "$attrs",
            "queryString",
            "$stateParams",
            "brandService",
            "$window",
            "sessionService"
        ];

        constructor(
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected $timeout: ng.ITimeoutService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected settingsService: core.ISettingsService,
            protected $scope: ng.IScope,
            protected $attrs: IProductCarouselAttributes,
            protected queryString: common.IQueryStringService,
            protected $stateParams: IProductListStateParams,
            protected brandService: brands.IBrandService,
            protected $window: ng.IWindowService,
            protected sessionService: account.ISessionService) {
            super(cartService, productService, $timeout, addToWishlistPopupService, settingsService, $scope, $attrs, queryString, $stateParams, brandService, $window);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("ProductCarouselController", PlumbersProductCarouselController);
}