﻿module insite.layout {
    "use strict";

    export class PlumbersLayoutController extends LayoutController {
        billTrustUrl: string;

        static $inject = ["$window", "sessionService", "$sce"];

        constructor(
            protected $window: ng.IWindowService,
            protected sessionService: insite.account.ISessionService,
            protected $sce: ng.ISCEService) {
            super($window);
        }

        $onInit(): void {
            super.$onInit();
            this.getSession();
        }

        protected getSession(): void {
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(session: SessionModel): void {      
            this.billTrustUrl = session.properties.billTrustUrl;
        }

        protected getSessionFailed(error: any): void {
        }
    }

    angular
        .module("insite")
        .controller("LayoutController", PlumbersLayoutController);
}