﻿module plumbers.account {
    import account = insite.account;
    import core = insite.core;

    export class PlumbersSelectCustomerController extends account.SelectCustomerController {

        initCustomerAutocompletes(settingsCollection: core.SettingsCollection): void {
            super.initCustomerAutocompletes(settingsCollection);
            this.$scope.$on("vm.fulfillmentMethod", (newValue) => {
                this.onChangeDeliveryMethod();
                if (this.billTo?.id) {
                   this.shipToOptions.dataSource.read();
                }
            });
        }

        protected onShipToAutocompleteRead(options: kendo.data.DataSourceTransportReadOptions, customerSettings: any): void {
            this.spinnerService.show();

            var expandParams = "excludeshowall,validation" + (this.fulfillmentMethod === "PickUp" ? ",excludeonetime" : "");
            this.customerService.getShipTos(expandParams, this.shipToSearch, this.getDefaultPagination(), this.billTo.id).then(
                (shipToCollection: ShipToCollectionModel) => { this.getShipTosCompleted(options, customerSettings, shipToCollection); },
                (error: any) => { this.getShipTosFailed(error); });
        }
        
        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            super.getSettingsCompleted(settingsCollection);
            this.enableWarehousePickup = settingsCollection.accountSettings.enableWarehousePickup;
            const requireSelectCustomerOnSignIn = settingsCollection.accountSettings.requireSelectCustomerOnSignIn;
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(requireSelectCustomerOnSignIn, session); },
                (error: any) => { this.getSessionFailed(error); });

            this.initCustomerAutocompletes(settingsCollection);
        }

        protected getShipTosCompleted(options: kendo.data.DataSourceTransportReadOptions, customerSettings: any, shipToCollection: ShipToCollectionModel): void {
            super.getShipTosCompleted(options, customerSettings, shipToCollection);
            const shipTos = shipToCollection.shipTos;
            const shipToValues = ["{{vm.defaultPageSize}}", "{{vm.totalShipTosCount}}"];

            this.shipToOptions = {
                headerTemplate: this.renderMessage(shipToValues, "totalShipToCountTemplate"),
                dataSource: new kendo.data.DataSource({
                    serverFiltering: true,
                    serverPaging: true,
                    transport: {
                        read: (options: kendo.data.DataSourceTransportReadOptions) => {
                            this.onShipToAutocompleteRead(options, customerSettings);
                        }
                    }
                }),
                select: (event: kendo.ui.AutoCompleteSelectEvent) => {
                    this.onShipToAutocompleteSelect(event);
                },
                minLength: 0,
                dataTextField: "label",
                dataValueField: "id",
                placeholder: this.getShipToPlaceholder()
            };

            setTimeout(() => { options.success(shipTos); }, 0);
        }
    }

    angular
        .module("insite")
        .controller("SelectCustomerController", PlumbersSelectCustomerController);
}
