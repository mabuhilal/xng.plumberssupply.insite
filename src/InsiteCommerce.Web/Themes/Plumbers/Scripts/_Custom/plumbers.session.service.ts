﻿module plumbers.account {

    import account = insite.account;

    export class PlumbersSessionService extends account.SessionService {
        redirectAfterSelectCustomer(sessionModel: SessionModel, byPassAddressPage: boolean, dashboardUrl: string, returnUrl: string, checkoutAddressUrl: string, reviewAndPayUrl: string, addressesUrl: string, cartUrl: string, canCheckOut: boolean): void {
            //PLMS-281 If one-time is selected, send user where they can edit the one-time address
            if (sessionModel.shipTo.oneTimeAddress) {
                returnUrl = `${checkoutAddressUrl}?isOneTime=true`;
                
                if (this.coreService.isSafari()) {
                    returnUrl += "&_=" + new Date().getTime();
                }

                this.coreService.redirectToPath(returnUrl)
            } else {
                super.redirectAfterSelectCustomer(sessionModel, byPassAddressPage, dashboardUrl, returnUrl, checkoutAddressUrl, reviewAndPayUrl, addressesUrl, cartUrl, canCheckOut);
            }
        }
    }

    angular
        .module("insite")
        .service("sessionService", PlumbersSessionService);
}