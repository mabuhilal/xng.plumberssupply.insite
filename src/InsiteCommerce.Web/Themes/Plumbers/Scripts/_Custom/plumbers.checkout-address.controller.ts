﻿module plumbers.cart {
    import account = insite.account;
    import cart = insite.cart;    
    import common = insite.common;
    import core = insite.core;
    import customers = insite.customers;
    import websites = insite.websites;

    export interface IPlumbersCheckoutAddressControllerAttributes extends cart.ICheckoutAddressControllerAttributes {
        changeCustomerUrl: string;
        homePageUrl: string;
    }

    export class PlumbersCheckoutAddressController extends cart.CheckoutAddressController {
        isOneTime: boolean = false;

        static $inject = [
            "$scope",
            "$window",
            "cartService",
            "customerService",
            "websiteService",
            "coreService",
            "queryString",
            "accountService",
            "settingsService",
            "$timeout",
            "$q",
            "sessionService",
            "$localStorage",
            "$attrs",
            "$rootScope"
        ];

        constructor(
            protected $scope: cart.ICartScope,
            protected $window: ng.IWindowService,
            protected cartService: cart.ICartService,
            protected customerService: customers.ICustomerService,
            protected websiteService: websites.IWebsiteService,
            protected coreService: core.ICoreService,
            protected queryString: common.IQueryStringService,
            protected accountService: account.IAccountService,
            protected settingsService: core.ISettingsService,
            protected $timeout: ng.ITimeoutService,
            protected $q: ng.IQService,
            protected sessionService: account.SessionService,
            protected $localStorage: common.IWindowStorage,
            protected $attrs: IPlumbersCheckoutAddressControllerAttributes,
            protected $rootScope: ng.IRootScopeService) {
            super($scope,
                $window,
                cartService,
                customerService,
                websiteService,
                coreService,
                queryString,
                accountService,
                settingsService,
                $timeout,
                $q,
                sessionService,
                $localStorage,
                $attrs,
                $rootScope);
        }

        $onInit(): void {
            super.$onInit();

            if (this.queryString.get("isOneTime") === "true") {
                this.isOneTime = true;
                this.editMode = true;
            }
        }

        protected setSelectedShipTo(): void {
            super.setSelectedShipTo();
            if (this.isOneTime) {
                for (let shipTo of this.shipTos) {
                    if (shipTo.oneTimeAddress) {
                        this.selectedShipTo = shipTo;
                        this.isReadOnly = false;
                        break;
                    }
                }
            }
        }

        continueCheckout(continueUri: string, cartUri: string): void {
            if (this.coreService.getReferringPath() === this.$attrs.changeCustomerUrl) {
                continueUri = this.$attrs.homePageUrl;
            }

            super.continueCheckout(continueUri, cartUri);
        }
    }

    angular
        .module("insite")
        .controller("CheckoutAddressController", PlumbersCheckoutAddressController);
}