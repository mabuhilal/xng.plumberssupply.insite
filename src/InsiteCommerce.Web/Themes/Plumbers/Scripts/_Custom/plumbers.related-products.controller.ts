﻿module insite.catalog {
    "use strict";

    export interface IRelatedProductsAttributes {
        carouselElementId: string;
    }

    export class PlumbersRelatedProductsController extends RelatedProductsController {
        canSeePrices: boolean;

        static $inject = ["cartService", "productService", "$timeout", "addToWishlistPopupService", "settingsService", "$scope", "$attrs", "sessionService"];

        constructor(
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected $timeout: ng.ITimeoutService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected settingsService: core.ISettingsService,
            protected $scope: ng.IScope,
            protected $attrs: IRelatedProductsAttributes,
            protected sessionService: account.ISessionService) {
            super(cartService, productService, $timeout, addToWishlistPopupService, settingsService, $scope, $attrs);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("RelatedProductsController", PlumbersRelatedProductsController);
}