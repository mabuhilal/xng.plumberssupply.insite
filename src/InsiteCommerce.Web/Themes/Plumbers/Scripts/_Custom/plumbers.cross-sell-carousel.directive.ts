﻿interface JQuery {
    flexslider: any;
}

module insite.catalog {
    "use strict";

    angular
        .module("insite")
        .directive("plumbersCrossSellCarousel", () => ({
            restrict: "E",
            replace: true,
            scope: {
                productCrossSell: "@",
                product: "=",
                maxTries: "@",
                canSeePrices: "="
            },
            templateUrl: "/PartialViews/Catalog-PlumbersCrossSellCarousel",
            controller: "CrossSellCarouselController",
            controllerAs: "vm",
            bindToController: true,
            link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, controller: CrossSellCarouselController) => {
                if (controller) {
                    controller.carouselElement = element;
                }
            }
        }));
}