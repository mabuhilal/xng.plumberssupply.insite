﻿module insite.savedorders {
    "use strict";

    export class PlumbersSavedOrderListController extends insite.savedorders.SavedOrderListController {
        canSeePrices: boolean;

        static $inject = ["cartService", "coreService", "paginationService", "settingsService", "sessionService"];

        constructor(
            protected cartService: cart.ICartService,
            protected coreService: core.ICoreService,
            protected paginationService: core.IPaginationService,
            protected settingsService: core.SettingsService,
            protected sessionService: account.ISessionService) {
            super(cartService, coreService, paginationService);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("SavedOrderListController", PlumbersSavedOrderListController);
}