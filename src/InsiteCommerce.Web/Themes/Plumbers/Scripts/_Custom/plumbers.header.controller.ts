﻿module insite.layout {
    "use strict";

    export class PlumbersHeaderController extends insite.layout.HeaderController {
        billTrustUrl: string;
        canSeePrices: boolean;

        static $inject = ["$scope", "$timeout", "cartService", "sessionService", "$window", "settingsService", "deliveryMethodPopupService", "$sce", "selectPickUpLocationPopupService"];

        constructor(
            protected $scope: ng.IScope,
            protected $timeout: ng.ITimeoutService,
            protected cartService: insite.cart.ICartService,
            protected sessionService: insite.account.ISessionService,
            protected $window: ng.IWindowService,
            protected settingsService: insite.core.ISettingsService,
            protected deliveryMethodPopupService: insite.account.IDeliveryMethodPopupService,
            protected $sce: ng.ISCEService,
            protected selectPickUpLocationPopupService: account.ISelectPickUpLocationPopupService) {
            super($scope, $timeout, cartService, sessionService, $window, settingsService, deliveryMethodPopupService);
        }

        $onInit(): void {
            super.$onInit();
        }

        protected getSessionCompleted(session: SessionModel): void {          
            super.getSessionCompleted(session);
            this.billTrustUrl = this.$sce.trustAsResourceUrl(session.properties.billTrustUrl);
            this.session = session;
            this.canSeePrices = session.userRoles && session.userRoles.length > 0 ? !session.userRoles.includes('Employee') : false;
        }

        protected openWarehouseSelectionModal(): void {
            this.selectPickUpLocationPopupService.display({
                session: this.session,
                updateSessionOnSelect: true,
                selectedWarehouse: this.session.pickUpWarehouse,
                onSelectWarehouse: (warehouse: WarehouseModel, onSessionUpdate?: Function) => this.updateSession(warehouse, onSessionUpdate)
            });
        }

        updateSession(warehouse: WarehouseModel, onSessionUpdate?: Function): void {
            const session = {} as SessionModel;
            session.pickUpWarehouse = warehouse;
            this.sessionService.updateSession(session).then(
                (updatedSession: SessionModel) => { this.updateSessionCompleted(updatedSession, onSessionUpdate); },
                (error: any) => { this.updateSessionFailed(error); });
        }

        protected updateSessionCompleted(session: SessionModel, onSessionUpdate?: Function): void {
            this.session = session;
            if (angular.isFunction(onSessionUpdate)) {
                onSessionUpdate();
            }
        }

        protected updateSessionFailed(error: any): void {
        }
    }

    angular
        .module("insite")
        .controller("HeaderController", PlumbersHeaderController);
}