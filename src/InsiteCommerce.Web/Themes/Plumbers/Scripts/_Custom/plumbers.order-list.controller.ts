﻿module insite.order {
    "use strict";

    export class PlumbersOrderListController extends OrderListController { 
        canSeePrices: boolean;

        static $inject = ["orderService", "customerService", "coreService", "paginationService", "settingsService", "searchService", "sessionService"];

        constructor(
            protected orderService: order.IOrderService,
            protected customerService: customers.ICustomerService,
            protected coreService: core.ICoreService,
            protected paginationService: core.IPaginationService,
            protected settingsService: core.ISettingsService,
            protected searchService: catalog.ISearchService,
            protected sessionService: account.ISessionService) {
            super(orderService, customerService, coreService, paginationService, settingsService, searchService);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("OrderListController", PlumbersOrderListController);
}