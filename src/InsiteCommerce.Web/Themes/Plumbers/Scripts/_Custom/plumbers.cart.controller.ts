﻿module insite.cart {
    "use strict";

    export class PlumbersCartController extends insite.cart.CartController {
        cart: CartModel;
        cartIdParam: string;
        promotions: PromotionModel[];
        settings: CartSettingsModel;
        canSeePrices: boolean;
        canAddToCart: boolean = true;
        showInventoryAvailability = false;
        productsCannotBePurchased = false;
        requiresRealTimeInventory = false;
        failedToGetRealTimeInventory = false;
        canAddAllToList = false;
        requisitionSubmitting = false;
        enableWarehousePickup = false;
        fulfillmentMethod: string;
        pickUpWarehouse: WarehouseModel;
        session: SessionModel;

        static $inject = ["$scope", "cartService", "promotionService", "settingsService", "coreService", "$localStorage", "addToWishlistPopupService", "spinnerService", "sessionService"];

        constructor(
            protected $scope: ICartScope,
            protected cartService: ICartService,
            protected promotionService: promotions.IPromotionService,
            protected settingsService: core.ISettingsService,
            protected coreService: core.ICoreService,
            protected $localStorage: common.IWindowStorage,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected spinnerService: core.ISpinnerService,
            protected sessionService: account.ISessionService) {
            super($scope, cartService, promotionService, settingsService, coreService, $localStorage, addToWishlistPopupService, spinnerService, sessionService);
        }

        $onInit(): void {
            super.$onInit();
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            super.getSessionCompleted(sessionModel);
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getCart(): void {
            this.cartService.expand = "cartlines,costcodes,hiddenproducts";
            if (this.settings.showTaxAndShipping) {
                this.cartService.expand += ",tax";
                this.cartService.allowInvalidAddress = true;
            }
            const hasRestrictedProducts = this.$localStorage.get("hasRestrictedProducts");
            if (hasRestrictedProducts === true.toString()) {
                this.cartService.expand += ",restrictions";
            }
            this.spinnerService.show();
            this.cartService.getCart().then(
                (cart: CartModel) => { this.getCartCompleted(cart); },
                (error: any) => { this.getCartFailed(error); });
        }

        protected getCartCompleted(cart: CartModel): void {
            super.getCartCompleted(cart);
            if (cart.cartLines.some(o => o.pricing.unitNetPrice <= 0)) {
                this.canAddToCart = false;
                this.cart.canCheckOut = false;
            }
        }
    }

    angular
        .module("insite")
        .controller("CartController", PlumbersCartController);
}