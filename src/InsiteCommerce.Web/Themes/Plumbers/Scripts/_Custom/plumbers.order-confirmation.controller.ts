﻿module insite.cart {
    "use strict";
    import StateModel = Insite.Websites.WebApi.V1.ApiModels.StateModel;
    import CountryModel = Insite.Websites.WebApi.V1.ApiModels.CountryModel;

    export class PlumbersOrderConfirmationController extends insite.cart.OrderConfirmationController {
        canSeePrices: boolean;

        static $inject = ["cartService", "promotionService", "queryString", "orderService", "sessionService", "settingsService", "addToWishlistPopupService"];

        constructor(
            protected cartService: ICartService,
            protected promotionService: promotions.IPromotionService,
            protected queryString: common.IQueryStringService,
            protected orderService: order.IOrderService,
            protected sessionService: account.ISessionService,
            protected settingsService: core.ISettingsService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService) {
            super(cartService, promotionService, queryString, orderService, sessionService, settingsService, addToWishlistPopupService);
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            super.getSettingsCompleted(settingsCollection);
            
            this.settings = settingsCollection.accountSettings;
        }
    }

    angular
        .module("insite")
        .controller("OrderConfirmationController", PlumbersOrderConfirmationController);
}