﻿module insite.order {
    "use strict";

    angular
        .module("insite")
        .directive("plumbersRecentOrders", () => ({
            controller: "RecentOrdersController",
            controllerAs: "vm",
            replace: true,
            restrict: "E",
            scope: {
                canSeePrices: "="
            },
            templateUrl: "/PartialViews/History-PlumbersRecentOrders"
        }));
}