﻿module insite.catalog {
    "use strict";

    angular
        .module("insite")
        .directive("plumbersCatalogBreadcrumb", () => ({
            restrict: "E",
            replace: true,
            scope: {
                breadcrumbs: "=",
                searchQuery: "="
            },
            templateUrl: "/PartialViews/Catalog-PlumbersBreadCrumb"
        }));
}