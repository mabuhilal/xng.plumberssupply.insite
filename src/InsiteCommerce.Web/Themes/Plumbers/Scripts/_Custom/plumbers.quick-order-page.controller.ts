﻿module insite.quickorder {
    "use strict";

    export class PlumbersQuickOrderPageController extends QuickOrderPageController {
        canSeePrices: boolean;

        static $inject = ["$scope", "$filter", "coreService", "cartService", "productService", "searchService", "settingsService", "addToWishlistPopupService", "selectVariantProductPopupService", "$q", "sessionService"];

        constructor(
            protected $scope: ng.IScope,
            protected $filter: ng.IFilterService,
            protected coreService: core.ICoreService,
            protected cartService: cart.ICartService,
            protected productService: catalog.IProductService,
            protected searchService: catalog.ISearchService,
            protected settingsService: core.ISettingsService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected selectVariantProductPopupService: SelectVariantProductPopupService,
            protected $q: ng.IQService,
            protected sessionService: account.ISessionService) {
            super($scope, $filter, coreService, cartService, productService, searchService, settingsService, addToWishlistPopupService, selectVariantProductPopupService, $q);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("QuickOrderPageController", QuickOrderPageController);
}