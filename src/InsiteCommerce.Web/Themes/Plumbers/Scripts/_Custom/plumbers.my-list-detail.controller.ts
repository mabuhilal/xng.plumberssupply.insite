﻿module insite.wishlist {
    "use strict";

    export class PlumbersMyListDetailController extends MyListDetailController {
        listId: string;
        invite: string;
        listModel: WishListModel;
        listSettings: WishListSettingsModel;
        productSettings: ProductSettingsModel;
        inProgress = false;
        checkStorage: {} = {};
        checkedItemsCount = 0;
        canPutAnySelectedToCart = false;
        currencySymbol: string;
        listTotal: number = 0;
        selectedListLines: WishListLineModel[];
        session: SessionModel;
        failedToGetRealTimePrices = false;
        failedToGetRealTimeInventory = false;
        sortableOptions: any;
        sortProperty: string = "sortOrder";
        isSortingMode = false;
        reverse: boolean = false;
        searchTerm: string = "";
        inviteIsNotAvailable: boolean;
        getListErrorMessage: string;

        addingSearchTerm: string = "";
        successMessage: string;
        errorMessage: string;
        itemToAdd: ProductDto;
        selectedQty: number;
        autocompleteOptions: AutoCompleteOptions;
        isAddToListSectionVisible: boolean = false;
        isAddingToList: boolean = false;
        messageTimeout: ng.IPromise<any>;

        notAvailableProducts: WishListLineModel[];

        noteForm: any;
        editNote: boolean;
        listLineNote: string;
        noteErrorMessage: string;
        orderIsSaving: boolean;
        myListUrl: string;
        containsInvalidLine: boolean = false;

        pagination: PaginationModel;
        paginationStorageKey = "DefaultPagination-MyListDetail";

        changedSharedListLinesQtys: { [key: string]: number } = {};
        listLinesWithUpdatedQty: { [key: string]: boolean } = {};

        static $inject = [
            "$scope",
            "settingsService",
            "queryString",
            "wishListService",
            "cartService",
            "productService",
            "sessionService",
            "$timeout",
            "$interval",
            "coreService",
            "spinnerService",
            "$location",
            "shareListPopupService",
            "uploadToListPopupService",
            "$localStorage",
            "searchService",
            "productPriceService",
            "paginationService",
            "$templateCache",
            "scheduleReminderPopupService",
            "createListPopupService",
            "deleteListPopupService",
            "copyToListPopupService",
            "listQuantityAdjustmentPopupService",
        ];

        constructor(
            protected $scope: ng.IScope,
            protected settingsService: core.ISettingsService,
            protected queryString: common.IQueryStringService,
            protected wishListService: IWishListService,
            protected cartService: cart.ICartService,
            protected productService: catalog.IProductService,
            protected sessionService: account.ISessionService,
            protected $timeout: ng.ITimeoutService,
            protected $interval: ng.IIntervalService,
            protected coreService: core.ICoreService,
            protected spinnerService: core.ISpinnerService,
            protected $location: ng.ILocationService,
            protected shareListPopupService: IShareListPopupService,
            protected uploadToListPopupService: IUploadToListPopupService,
            protected $localStorage: common.IWindowStorage,
            protected searchService: catalog.ISearchService,
            protected productPriceService: catalog.IProductPriceService,
            protected paginationService: core.IPaginationService,
            protected $templateCache: ng.ITemplateCacheService,
            protected scheduleReminderPopupService: IUploadToListPopupService,
            protected createListPopupService: ICreateListPopupService,
            protected deleteListPopupService: IDeleteListPopupService,
            protected copyToListPopupService: ICopyToListPopupService,
            protected listQuantityAdjustmentPopupService: IListQuantityAdjustmentPopupService) {
            super($scope, settingsService, queryString, wishListService, cartService, productService, sessionService, $timeout, $interval, coreService, spinnerService, $location,
                shareListPopupService, uploadToListPopupService, $localStorage, searchService, productPriceService, paginationService, $templateCache, scheduleReminderPopupService,
                createListPopupService, deleteListPopupService, copyToListPopupService, listQuantityAdjustmentPopupService)
        }

        $onInit(): void {
            super.$onInit();            
        }

        protected handleRealTimePricesCompleted(result: RealTimePricingModel): void {
            this.failedToGetRealTimePrices = false;
            result.realTimePricingResults.forEach((productPrice: ProductPriceDto) => {
                const wishlistLine = this.listModel.wishListLineCollection.find(
                    (p: WishListLineModel) => p.productId === productPrice.productId &&
                        p.unitOfMeasure === productPrice.unitOfMeasure);

                if (wishlistLine && wishlistLine.pricing) {
                    wishlistLine.pricing = productPrice
                }

                if (wishlistLine && wishlistLine.pricing && wishlistLine.pricing.unitNetPrice <= 0) {
                    this.containsInvalidLine = true;
                }
            });

            this.calculateListTotal();

            if (this.productSettings.inventoryIncludedWithPricing) {
                this.getRealTimeInventory();
            }
        }

        selectAll(): void {
            if (this.isAllSelected()) {
                this.checkStorage = {};
            } else {
                for (let i = 0; i < this.listModel.wishListLineCollection.length; i++) {
                    if (this.listModel.wishListLineCollection[i].pricing.unitNetPrice > 0) {
                        this.checkStorage[this.listModel.wishListLineCollection[i].id.toString()] = true;
                    }                    
                }
            }
        }

    }

    angular
        .module("insite")
        .controller("MyListDetailController", PlumbersMyListDetailController);
}