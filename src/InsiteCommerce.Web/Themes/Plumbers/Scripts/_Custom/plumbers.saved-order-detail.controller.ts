﻿module plumbers.savedorders {
    "use strict";

    export class PlumbersSavedOrderDetailController extends insite.savedorders.SavedOrderDetailController {
        canSeePrices: boolean;

        static $inject = ["cartService", "coreService", "spinnerService", "settingsService", "queryString", "addToWishlistPopupService", "sessionService"];

        constructor(
            protected cartService: insite.cart.ICartService,
            protected coreService: insite.core.ICoreService,
            protected spinnerService: insite.core.ISpinnerService,
            protected settingsService: insite.core.ISettingsService,
            protected queryString: insite.common.IQueryStringService,
            protected addToWishlistPopupService: insite.wishlist.AddToWishlistPopupService,
            protected sessionService: insite.account.ISessionService) {
            super(cartService, coreService, spinnerService, settingsService, queryString, addToWishlistPopupService);
        }

        $onInit(): void {
            super.$onInit();
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(session); },
                (error: any) => { this.getSessionFailed(error); });
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected getSessionFailed(error: any) {
        }
    }

    angular
        .module("insite")
        .controller("SavedOrderDetailController", PlumbersSavedOrderDetailController);
}