﻿module insite.dashboard {
    "use strict";

    export class PlumbersRecentOrdersController extends insite.order.RecentOrdersController {
        canSeePrices: boolean;

        $onInit(): void {
            super.$onInit();
        }

        protected getSessionCompleted(sessionModel: SessionModel) {
            super.getSessionCompleted(sessionModel);
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }
    }

    angular
        .module("insite")
        .controller("RecentOrdersController", PlumbersRecentOrdersController);
}