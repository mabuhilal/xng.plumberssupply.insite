﻿module plumbers.catalog {
    "use strict";

    export class PlumbersCategoryLeftNavController extends insite.catalog.CategoryLeftNavController {
        facetListLimit: number = 5;
        facetBrandsListLimit: number = 5;

        protected showMoreCategories(categoryLength: number): void {            
            this.facetListLimit = categoryLength;
        }

        protected showMoreBrands(brandsLength: number): void {
            this.facetBrandsListLimit = brandsLength;
        }
    }

    angular
        .module("insite")
        .controller("CategoryLeftNavController", PlumbersCategoryLeftNavController);
}