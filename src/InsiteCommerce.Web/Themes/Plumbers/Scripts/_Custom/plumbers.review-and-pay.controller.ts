﻿module insite.cart {
    "use strict";

    export class PlumbersReviewAndPayController extends insite.cart.ReviewAndPayController {
        placedByPhoneNumber: string;
        canSeePrices: boolean;
        noPaymentOptionsAvailable: boolean;
        allItemsFulfilled: boolean;
        isWholesaleCustomerSegment: boolean = false;

        protected submitCart(): void {
            if (this.placedByPhoneNumber) {
                this.cart.billTo.properties["placedByPhoneNumber"] = this.placedByPhoneNumber;
            }
            
            super.submitCart();
        }

        getCart(isInit?: boolean): void {
            this.spinnerService.show();
            this.cartService.expand = `cartlines,carriers,paymentoptions,${Date.now()}`; // include date to prevent caching when clicking back from order confirmation
            if (this.$localStorage.get("hasRestrictedProducts") === true.toString()) {
                this.cartService.expand += ",restrictions";
            }
            this.cartService.forceRecalculation = true;
            this.cartService.allowInvalidAddress = true;
            this.cartService.getCart(this.cartId).then(
                (cart: CartModel) => {
                    this.getCartCompleted(cart, isInit);
                },
                (error: any) => {
                    this.getCartFailed(error);
                });
        }


        protected getSessionCompleted(sessionModel: SessionModel) {
            super.getSessionCompleted(sessionModel);
            this.canSeePrices = sessionModel.userRoles && sessionModel.userRoles.length > 0 ? !sessionModel.userRoles.includes('Employee') : false;
        }

        protected setUpPaymentMethod(isInit: boolean, selectedMethod: Insite.Cart.Services.Dtos.PaymentMethodDto): void {
            if (this.cart.paymentOptions.paymentMethods.length < 1) {
                this.noPaymentOptionsAvailable = true;
                return;
            }

            if (!selectedMethod) {
                selectedMethod = this.cart.paymentOptions.paymentMethods[0];
            }
            super.setUpPaymentMethod(isInit, selectedMethod);
            this.noPaymentOptionsAvailable = (this.cart.paymentMethod === null || this.cart.paymentMethod === undefined);
        }

        protected getCartCompleted(cart: CartModel, isInit: boolean): void {
            super.getCartCompleted(cart, isInit);
            this.allItemsFulfilled = !cart.cartLines.some(o => o.qtyOnHand < o.qtyOrdered);  
            this.isWholesaleCustomerSegment = this.cart.billTo.properties.plumbers_customerSegment === "Wholesale";
        }
    }

    angular
        .module("insite")
        .controller("ReviewAndPayController", PlumbersReviewAndPayController);
}